import argparse
import csv
import logging
import os
import sys
import string

from zipfile import ZipFile

# set up parsing here
# must specify a directory of zip files 
parser = argparse.ArgumentParser(description='Process watchlist hits from MSS.')
parser.add_argument('directory', help='Directory of zip files to process')
parser.add_argument('output', help='Output file')
parser.add_argument('-l','--logfile', help='Log file if STDERR not desired')
args = parser.parse_args()

# set up logging here (for debugging purposes)
if args.logfile:
    logging.basicConfig(filename=args.logfile, level=logging.DEBUG, 
                        format='%(asctime)s %(message)s', 
                        datefmt='%Y-%m-%d %H:%M:%S')
else:
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s', 
                        datefmt='%Y-%m-%d %H:%M:%S')

directory = args.directory
try:
    filelist = [ os.path.join(directory,f) for f in os.listdir(directory) if os.path.isfile(os.path.join(directory,f)) ]
except OSError:
    logging.critical('Could not read directory %s' % directory)
    raise

# this will consist of a list of lists
# where each individual list is from each SOC location
hitdata = list()

# for each file in the set, uncompress
for f in filelist:
    with ZipFile(f,'r') as msszip:
        for datafile in msszip.namelist():
            row=list()
            data=list()
            with msszip.open(datafile,'r') as csvfile:
                datareader = csv.reader(csvfile)
                for row in datareader:
                    # nest ALL the things!
                    data.append(row)
                hitdata.append(data)

alldata = dict()
for soc in hitdata:
    socdata = dict()
    for row in soc:
        if not (row[2] in socdata):
            # dict of lists, where key is address and data is [watchlist, hits]
            socdata[row[2]] = [':'.join((row[0],row[1])),row[3]]
        else:
            # oh my god this is ugly
            socdata[row[2]][0] = (' '.join((socdata[row[2]][0],':'.join((row[0],row[1])))))
    for ip in socdata:
        if ip in alldata:
            alldata[ip]['count'] += int(socdata[ip][1])
            for s in string.split(socdata[ip][0]):
                if string.count(alldata[ip]['sources'],s) == 0:
                    alldata[ip]['sources'] = ' '.join((s,alldata[ip]['sources']))
        elif ip != "ipaddress":
            alldata[ip] = {'sources': socdata[ip][0], 'count': int(socdata[ip][1])}

with open(args.output,'wb') as outfile:
    writer = csv.writer(outfile)
    for d in alldata:
        writer.writerow((d,alldata[d]['sources'],alldata[d]['count']))
