iocdbREPORT
=======
Reporting for IOCDB (daily). **iocdbREPORT.py** takes in a daily DB dump of observables submitted to IOCDB and collates said properties of observables into daily HTML reports using R.

##Installation Procedure
* **Prepare directory structure**
```
#::Reporting directory::#
mkdir /var/www/ (Apache default)
mkdir /var/www/KNOWN_SOURCES (for caching feed sources)
mkdir /opt/reporting/archive (data archive)
mkdir /opt/reporting/rscripts (rscripts)
mkdir /opt/reporting/data (staging directory)
```

* **Install R**
```
Ideally...
apt-get install r-base
apt-get install r-base-dev

Worst case...
echo "deb http://cran.r-project.org/bin/linux/ubuntu precise/" >> /etc/apt/sources.list
gpg --keyserver subkeys.pgp.net --recv 51716619E084DAB9
gpg --keyserver keyserver.ubuntu.com --recv E084DAB9  
gpg --export --armor E084DAB9 | sudo apt-key add - && sudo apt-get update
apt-get update
apt-get install r-base r-base-dev
```

* **Install R Packages**
```
install.packages("plyr")
install.packages("ggplot2")
install.packages("scales")
install.packages("grid")
install.packages("plyr")
install.packages("RColorBrewer")
install.packages("knitr")
```

* **Install Maxmind Support**
 * Install C-Library: http://www.maxmind.com/download/geoip/api/c/GeoIP-latest.tar.gz
 * Install Python Package: https://github.com/maxmind/geoip-api-python
 * Load Databases in: /usr/local/share/GeoIP/ (see pyGEO)
 
* **Copy scripts to server**
 * iocdbREPORT.py to /opt/reporting/
 * daily.Rmd to /opt/reporting/rscripts/

## Input Expections:
iocdbREPORT.py expects a comma delimited data feed with the following format:
observables.value, observables.variety, observables.valid, documents.name, documents.source, documents.category, documents.tlp, documents.investigator, ttps.category, ttps.actions

## Usage Example:
python iocdbREPORT.py --db-dump=/var/data/mydatabasedump.csv --report-dir=/var/www/REPORTS --archive-dir=/opt/reporting/archive --stage-dir=/opt/reporting/data --known-sources=/var/www/KNOWN_SOURCES
