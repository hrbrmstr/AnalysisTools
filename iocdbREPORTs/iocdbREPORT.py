#!/usr/bin/env python

import os,sys
import csv
import GeoIP
import collections
import time
import shutil
from argparse import ArgumentParser

class processIOC_daily(object):
	def __init__(self,ioc_type):
		self.investigator_count = collections.defaultdict(int)
		self.category_count = collections.defaultdict(int)
		self.hourly_by_feed = dict()
		self.hourly_by_variety = dict()
		self.hourly_total = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		self.variety_count = collections.defaultdict(int)
		self.ttp_category = collections.defaultdict(int)
		self.ttp_actions = collections.defaultdict(int)
		self.known_sources_file=None
		self.initial_sources=[]
		self.count_by_feed=collections.defaultdict(int)

		#::Read in known sources::#
		try:
			self.known_sources_file=open(os.path.join(KNOWN_SOURCES,ioc_type + "-KNOWN_SOURCES.txt"),'w+b')
			for line in f:
				if not len(line) == 0:
					FEED=line.rstrip()
					self.initial_sources.append(FEED)
					self.hourly_by_feed[FEED] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		except:
			try:
				if not os.path.exists(os.path.join(KNOWN_SOURCES,ioc_type + "-KNOWN_SOURCES.txt")):
					self.known_sources_file=open(os.path.join(KNOWN_SOURCES,ioc_type + "-KNOWN_SOURCES.txt"),"w+b")
			except:
				print "[!] - You've got some issues loading up the KNOWN_SOURCES data..."

	def update_feedHourly(self,feed,hour):
		if self.hourly_by_feed.has_key(feed):
			self.hourly_by_feed[feed][hour] += 1
		else:
			self.hourly_by_feed[feed] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			self.hourly_by_feed[feed][hour] += 1

	def update_varietyHourly(self,variety,hour):
		if self.hourly_by_variety.has_key(variety):
			self.hourly_by_variety[variety][hour] += 1
		else:
			self.hourly_by_variety[variety] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			self.hourly_by_variety[variety][hour] += 1


def produce_jay(feed_ip,feed_name,feed_category,feed_action,feed_valid): #::produces csv of IOCs per jay's original R-script
	asn=geoIP_asn.name_by_addr(feed_ip)
	country=geoIP_country.country_name_by_addr(feed_ip)
	cc_code=geoIP_country.country_code_by_addr(feed_ip)

	if asn == None:
		asn = "RFC 1918"
		country = "RFC 1918"
		cc_code = "RFC 1918"

	jWRITER.writerow([feed_ip,feed_name,feed_category,feed_action,'3179771043',asn,cc_code,country,feed_valid])

def produce_like_jay(feed_ip,feed_name,feed_category,feed_action,feed_valid): #::produces csv of IOCs per jay's original R-script
	dWRITER.writerow([feed_ip,feed_name,feed_category,feed_action,"placeholding","asn","UK","United Kingdom",feed_valid])

def dump_reports(): #::dumps report summaries

	def gen_report_produce():
		for objVariety,varietyStruct in REPORT_OBJECTS.items():
			#::investigator::#
			output=os.path.join(RAW_DATA,objVariety + "-investigator.csv")
			openme=open(output,'w+b')
			iwriter=csv.writer(openme,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
			iwriter.writerow(['investigator','count'])
			for key,value in varietyStruct.investigator_count.items():
				if key == "":
					ignore=True
				else:
					iwriter.writerow([key,value])

			#::category_count::#
			output=os.path.join(RAW_DATA,objVariety + "-document_category.csv")
			openme=open(output,'w+b')
			iwriter=csv.writer(openme,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
			iwriter.writerow(['document_category','count'])
			for key,value in varietyStruct.category_count.items():
				if key == "":
					key = "unspecified"

				iwriter.writerow([key,value])

			#::hourly by feed::#
			output=os.path.join(RAW_DATA,objVariety + "-feed_hourly.csv")
			openme=open(output,'w+b')
			iwriter=csv.writer(openme,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
			iwriter.writerow(['feed','zero','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'])
			for key,value in varietyStruct.hourly_by_feed.items():
				value.insert(0,key)
				iwriter.writerow(value)

			#::hourly by variety::#
			output=os.path.join(RAW_DATA,objVariety + "-variety_hourly.csv")
			openme=open(output,'w+b')
			iwriter=csv.writer(openme,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
			iwriter.writerow(['feed','zero','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'])
			for key,value in varietyStruct.hourly_by_variety.items():
				value.insert(0,key)
				iwriter.writerow(value)

			#::hourly by variety::#
			output=os.path.join(RAW_DATA,objVariety + "-hourly_total.csv")
			openme=open(output,'w+b')
			iwriter=csv.writer(openme,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
			iwriter.writerow(['hour','count'])
			for i in range(len(varietyStruct.hourly_total)):
				iwriter.writerow([str(i),varietyStruct.hourly_total[i]])

			#::TTP category counts::#
			output=os.path.join(RAW_DATA,objVariety + "-ttp_category.csv")
			openme=open(output,'w+b')
			iwriter=csv.writer(openme,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
			iwriter.writerow(['ttp_category','count'])
			for key,value in varietyStruct.ttp_category.items():
				if key == "":
					key = "unspecified"

				iwriter.writerow([key,value])

			#::TTP action counts::#
			output=os.path.join(RAW_DATA,objVariety + "-ttp_actions.csv")
			openme=open(output,'w+b')
			iwriter=csv.writer(openme,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
			iwriter.writerow(['ttp_actions','count'])
			for key,value in varietyStruct.ttp_actions.items():
				if key == "":
					key = "action_unspecified"

				iwriter.writerow([key,value])

			#::Count by feed::#
			output=os.path.join(RAW_DATA,objVariety + "-count_by_feed.csv")
			openme=open(output,'w+b')
			iwriter=csv.writer(openme,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
			iwriter.writerow(['feed','count'])
			for key,value in varietyStruct.count_by_feed.items():
				if key == "":
					key = "unspecified"

				iwriter.writerow([key,value])


	#::Planned to report generic stuff first::#
	gen_report_produce()

	#::other more specific stuff here::#
	#::variety_count::#
	output=os.path.join(RAW_DATA,"overall-variety_count.csv")
	openme=open(output,'w+b')
	iwriter=csv.writer(openme,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
	iwriter.writerow(['variety','count'])
	for key,value in REPORT_OBJECTS['overall'].variety_count.items():
		if key == "":
			key = "unspecified"
		iwriter.writerow([key,value])

def update_knownSources():
	for objVariety,varietyStruct in REPORT_OBJECTS.items():
		for key,value in varietyStruct.hourly_by_feed.items():
			if not key in varietyStruct.initial_sources:
				varietyStruct.known_sources_file.write(key + "\n")

def create_stage(DATA_OUTPUTS): #::Creates staging directory

	for reporting_dir in DATA_OUTPUTS:
		if not os.path.exists(reporting_dir):
			try:
				os.makedirs(reporting_dir)
			except:
				print "[!] - ERROR: Couldn't create: %s ...exiting" % (reporting_dir)
				sys.exit(0)

def purgatory_DAILY(db_dump): #::Because main() is really boring.

	#::read through file::#
	try:
		INPUT=open(db_dump,"rb")
	except:
		print "[!] - ERROR: Couldn't open up DB Dump: %s ...exiting" % (db_dump)
		sys.exit(0)

	#::Overall reporting object::#
	REPORT_OBJECTS['overall']=processIOC_daily('overall')
	REPORT_OBJECTS['investigator']=processIOC_daily('investigator')

	reader=csv.reader(INPUT)
	for line in reader:
		observable=line[0]
		variety=line[1]
		valid=line[2]
		name=line[3] #::likely to be URL
		source=line[4] #::likely to be a labeled description
		category=line[5] #::csint,osint,vzir
		tlp=line[6]
		investigator=line[7]
		ttp_category=line[8]
		ttp_actions=line[9]

		#::Determine hour of feed insertion::#
		valid_break=valid.split(' ')
		valid_time=valid_break[1].split(':')
		date=str(valid_break[0])
		hour=int(valid_time[0])

		if ttp_category == "" or ttp_category == None:
				ttp_category = 'category_unspecified'
		elif ttp_actions == "" or ttp_actions == None:
				ttp_actions = 'action_unspecified'

		#::jreport::#
		if variety == "ip":
			produce_jay(observable,name,ttp_category,ttp_actions,date)
		elif variety == "domain":
			produce_like_jay(observable,name,ttp_category,ttp_actions,date)

		#::if variety is not blank - we ain't play'n games here, son!!!::#
		if not len(variety) == 0:
			if not REPORT_OBJECTS.has_key(variety) and not len(variety) < 1:
				REPORT_OBJECTS[variety]=processIOC_daily(variety)

			#::Hourly::#
			REPORT_OBJECTS['overall'].update_varietyHourly(variety,hour)

			if source == "":
				REPORT_OBJECTS['overall'].update_feedHourly(name,hour)
				REPORT_OBJECTS[variety].update_feedHourly(name,hour)
			else:
				REPORT_OBJECTS['overall'].update_feedHourly(source,hour)
				REPORT_OBJECTS[variety].update_feedHourly(source,hour)
			
			#::Investigator Stats::#

			REPORT_OBJECTS['overall'].investigator_count[investigator] += 1
			REPORT_OBJECTS[variety].investigator_count[investigator] += 1

			#::Category stats::#
			REPORT_OBJECTS['overall'].category_count[category] += 1
			REPORT_OBJECTS[variety].category_count[category] += 1	

			#::Variety Count::#
			REPORT_OBJECTS['overall'].variety_count[variety] += 1

			#::TTP category Count::#
			REPORT_OBJECTS['overall'].ttp_category[ttp_category] += 1
			REPORT_OBJECTS[variety].ttp_category[ttp_category] += 1

			#::TTP Action Count::#
			REPORT_OBJECTS['overall'].ttp_actions[ttp_actions] += 1
			REPORT_OBJECTS[variety].ttp_actions[ttp_actions] += 1

			#::Update hourly count::#
			REPORT_OBJECTS['overall'].update_feedHourly(name,hour)
			REPORT_OBJECTS[variety].update_feedHourly(name,hour)

			#::Feed by Hour::#
			REPORT_OBJECTS['overall'].hourly_total[hour] += 1
			REPORT_OBJECTS[variety].hourly_total[hour] += 1

			#::Count by feed::#
			REPORT_OBJECTS['overall'].count_by_feed[source] += 1
			REPORT_OBJECTS[variety].count_by_feed[source] += 1			

	print "[+] - Updating known sources..."
	update_knownSources()
	print "[+] - Dumping Reports..."
	dump_reports()
	print "[+] - Running R..."
	os.system("Rscript -e \"library(knitr); knit2html('/opt/reporting/rscripts/daily.Rmd', output='report.html')\"")
	print "[+] - Moving staged data to reporting directory..."
	shutil.move(RAW_DATA,REPORT_DIR)
	shutil.move("report.html",REPORT_DIR)
	shutil.move("figure",REPORT_DIR)

	print "[+] - Done..."

def main():

	global geoIP_country
	global geoIP_asn

	geoIP_country = GeoIP.open("/usr/local/share/GeoIP/GeoIP.dat",GeoIP.GEOIP_MEMORY_CACHE)
	geoIP_asn = GeoIP.open("/usr/local/share/GeoIP/GeoIPASNum.dat",GeoIP.GEOIP_MEMORY_CACHE)

	global CURRENT_DATE
	global REPORT_DIR
	global ARCHIVE_DIR
	global RAW_DATA
	global KNOWN_SOURCES
	global REPORT_OBJECTS
	global STAGE_DIR
	global jWRITER
	global dWRITER

	CURRENT_DATE = time.strftime("%Y-%m-%d")

	REPORT_OBJECTS = dict()

	p = ArgumentParser(description='prepREPORT.py',usage='-h for a better description')
	p.add_argument('--db-dump',action='store',dest='iocs',help='File to Parse',required=True)
	p.add_argument('--report-dir',action='store',dest='report_dir',help='Final/raw data report location. Provide absolute path.',required=True)
	p.add_argument('--archive-dir',action='store',dest='archive_dir',help='Where to archive data. Provide absolute path.',required=True)
	p.add_argument('--stage-dir',action='store',dest='stage_dir',help='Where to archive data. Provide absolute path.',required=True)
	p.add_argument('--known-sources',action='store',dest='known_sources',help='Where to archive data. Provide absolute path.',required=True)
	args = p.parse_args(sys.argv[1:])

	print "[+] - Staging output directories..."
	STAGE_DIR=args.stage_dir
	KNOWN_SOURCES = args.known_sources
	REPORT_DIR=os.path.join(args.report_dir,CURRENT_DATE)
	ARCHIVE_DIR=os.path.join(args.archive_dir,CURRENT_DATE)
	RAW_DATA=os.path.join(STAGE_DIR,"raw_data")
	JREPORT=os.path.join(RAW_DATA,"jreport.csv")

	#::STAGE OUTPUT::#
	create_stage([REPORT_DIR,ARCHIVE_DIR,RAW_DATA])

	#::JREPORT HANDLE::#
	wJREPORT=open(JREPORT,"w+b")
	jWRITER=csv.writer(wJREPORT,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
	dJREPORT=open(os.path.join(RAW_DATA,"jdreport.csv"),"w+b")
	dWRITER=csv.writer(dJREPORT,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)

	#::BEGIN ANALYSIS::#
	print "[+] - Collating data..."
	purgatory_DAILY(args.iocs)


if __name__ == '__main__':
        main()
