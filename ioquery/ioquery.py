from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan
import json
import argparse
import os



es = Elasticsearch(
    [os.environ['IOCDB_SERVER']],
    http_auth=(os.environ['IOCDB_USER'], os.environ['IOCDB_PASSWORD']),
    sniff_on_start=False,
    sniff_on_connection_fail=False,
    timeout = 20
)

query = {
        "query": { "match_all": {}},
        "filter": {"and": []}
    }


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Gonna get dirrty")
    parser.add_argument("--observable_values", help="what you looking for", nargs="+")
    parser.add_argument("--observable_varieties", help="what kind of indicators you want", nargs="+", choices=["ip","domain","url"])
    parser.add_argument("--document_sources", help="where you want the indicators to come from", nargs="+")
    parser.add_argument("--document_names", help="document you want to look for", nargs="+")
    parser.add_argument("-v", "--verbose", help="displays additional diagnostic information", action="store_true")
    args = parser.parse_args()
    if not (args.observable_values or
            args.observable_varieties or
            args.document_sources or
            args.document_names):
            parser.error("You gotta give me something to query")


    if args.observable_values:
        query['filter']['and'].append({"terms": {"observable.value": args.observable_values}})
    if args.observable_varieties:
        query['filter']['and'].append({"terms": {"observable.variety": args.observable_varieties }})
    if args.document_sources:
        query['filter']['and'].append({"terms": {"document.source": args.document_sources }})
    if args.document_names:
        query['filter']['and'].append({"terms": {"document.name.untouched": args.document_names }})


    if args.verbose:
        print(json.dumps(query, sort_keys=True, indent=2))

    res = scan(client=es, query=query,
               index='iocdb', doc_type='rumor')

    for row in res:
        print(json.dumps(row, separators=(',', ':')))
