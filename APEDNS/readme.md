APEDNS
=======
Asynchronous DNS resolvers based on adns-python to assist resolving a large number of domains in a short period of time.

          __
     w  c(..)o   (
      \__(-)    __)
          /\   (
         /(_)___)
         w /|
          | \
         m  m

##Current Tools:
* **apedns-batch.py** script used to batch resolve a list of domains/fqdns.
* **apedns-brute.py** script used to identify potentially active subdomains given a list of parent domains.
* **apedns.py** script that combines batch/brute search features.

##Usage:
* ```./apedns-batch.py --input=stdin/file.txt --output=stdout/output.txt```
* ```./apedns-brute.py --input=stdin/file.txt --subdomains=[subdomain1,subdomain2|file.txt] --output=stdout/output.txt```
* ```./apedns.py --rr="(A|NS|PTR)" --successful_only (optional) (batch|brute) --input=stdin/file.txt --subdomains=[sub1,sub2,sub3|file.txt] --output=stdout/output.txt```   

##Usage (in a script):
* You can use apedns in a script! **(see: apedns-example.py)**
* apedns expects domains to be passed to the constructor as a list.
* apedns can also handle output of domains to: a file (TO_FILE), stdout (STDOUT), or returned as a list (TO_LIST)

##Example:
* **Fields:** status_code,domain,ttl,response,cname
 * If a domain returns multiple records (i.e. round-robin) a list is returned for each response value.

```
mmatonis@iocdbdns:~/$ echo -e "google.com\nverizon.com\nthisdomaindoesnotexistnoritshouldnt.com" | python /opt/apedns/apedns.py --rr="A" batch --input=stdin --output=stdout
OK,google.com,4,74.125.196.102,NO_CNAME
OK,google.com,4,74.125.196.113,NO_CNAME
OK,google.com,4,74.125.196.101,NO_CNAME
OK,google.com,4,74.125.196.100,NO_CNAME
OK,google.com,4,74.125.196.139,NO_CNAME
OK,google.com,4,74.125.196.138,NO_CNAME
NXDOMAIN,thisdomaindoesnotexistnoritshouldnt.com,CHECK_STATUS,CHECK_STATUS,CHECK_STATUS
OK,verizon.com,600,192.76.85.245,NO_CNAME

mmatonis@iocdbdns:~/$ echo -e "google.com\nverizon.com\nthisdomaindoesnotexistnoritshouldnt.com" | python /opt/apedns/apedns.py --rr="A" brute --input=stdin --output=stdout --subdomains=test,www,thisiscrazy
NXDOMAIN,thisiscrazy.google.com,CHECK_STATUS,CHECK_STATUS,CHECK_STATUS
NXDOMAIN,test.verizon.com,CHECK_STATUS,CHECK_STATUS,CHECK_STATUS
OK,www.google.com,85,74.125.196.106,NO_CNAME
OK,www.google.com,85,74.125.196.105,NO_CNAME
OK,www.google.com,85,74.125.196.104,NO_CNAME
OK,www.google.com,85,74.125.196.147,NO_CNAME
OK,www.google.com,85,74.125.196.103,NO_CNAME
OK,www.google.com,85,74.125.196.99,NO_CNAME
NXDOMAIN,test.google.com,CHECK_STATUS,CHECK_STATUS,CHECK_STATUS
NXDOMAIN,thisiscrazy.verizon.com,CHECK_STATUS,CHECK_STATUS,CHECK_STATUS
NXDOMAIN,www.thisdomaindoesnotexistnoritshouldnt.com,CHECK_STATUS,CHECK_STATUS,CHECK_STATUS
NXDOMAIN,test.thisdomaindoesnotexistnoritshouldnt.com,CHECK_STATUS,CHECK_STATUS,CHECK_STATUS
CNAME is www.verizon.com.edgekey.net,www.verizon.com,CHECK_STATUS,CHECK_STATUS,CHECK_STATUS
NXDOMAIN,thisiscrazy.thisdomaindoesnotexistnoritshouldnt.com,CHECK_STATUS,CHECK_STATUS,CHECK_STATUS
OK,www.verizon.com.edgekey.net,13230,184.87.10.120,e2546.g.akamaiedge.net
```

##Installation
* **1. Install adns:** Download adns-1.2 from adns-classes and compile via: 

```
./configure && make && make install
```
* **2. Install adns-python:** Download adns-python: https://code.google.com/p/adns-python/
* **3. Install apedns module:** Download apedns-classes/apedns.py and copy to Python dist-packages directory.
* **4. Configure nameservers:** Add nameservers to /etc/resolv.conf && /etc/adns-resolv.conf
* **5. Use it!**

##Configuration:
* adns/adns-python reads nameservers from: /etc/resolv.conf & /etc/resolv-adns.conf
* Allowing for a maximum of 5 DNS servers (for failover and load balancing), adns extends libresolv by also reading additional nameservers from: /etc/resolv-adns.conf
* /etc/resolv-adns.conf follows the same resolv.conf format:
 * nameserver 4.2.2.1
 * nameserver 4.2.2.2

##DNS Server Considerations:
* Acting a stub resolver, apedns relies on having access to fast recursive name servers.
* Due to the prevalent abuse of open-recursive nameservers, many open-resolvers have chosen to rate limit incoming requests that may yield unreliable performance/results.
* The following Level 3 Communications servers have been most reliable during preliminary tests:
 * 4.2.2.1
 * 4.2.2.2
 * 4.2.2.3
 * 4.2.2.4
 * 4.2.2.5
* Tools such as namebench.py (Google Code) are useful in ascertaining regional domain servers that may be suitable for the high volume of requests that apedns.py delivers.
* Many DNS Servers may actively sinkhole threats or uniquely respond to NXDOMAINS with arbitrary answers. Therefore, it is important that this be identified as it may negatively influence analytical processes.

##Debug Logging:
* If chosen to be utilized in scripted/cron'd environment, apedns stores a runlog within the current working directory: apemessages.log

##Use Cases:
Apedns has at least two established use cases:
* Identifying IP addresses of malicious domains.
* Identifying unknown C2 domains within known-malicious namespaces based on keywords. (i.e. nccic,us-cert,noaa)
