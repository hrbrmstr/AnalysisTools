#!/usr/bin/python

from argparse import ArgumentParser
import argparse
import sys,os
import re
import adns
from time import time
import time
import pprint

class apeResolver(object):

	callback = None
	callback_submit = None

	def __init__(self,domain_list=True,rr_type=True,s=None):
		rr_code = {"A" : 1, "NS" : 2, "PTR" : 65548, "TXT" : 16 }
		self.hosts = domain_list
		self.query_type = rr_code[rr_type]
		self._queries = {} #::dictionary of active query objects, looped through by run()
		self._results = {} #::dictionary of tuples storing query results.
		self._final_list=[] #::This is our master list of lists.
		#self._s = s or adns.init(adns.iflags.noautosys) #::s is custom nameserver configuration
		self._s=adns.init()

	#::submit is called in a loop (resolve()) providing the following arguments::#
	def send_query(self, qname, rr, flags=0, callback=None, extra=None):
		callback = callback or self.callback_submit
		if not callback: raise Error, "callback required"
		q = self._s.submit(qname, rr, flags)
		self._queries[q] = qname, rr, flags, callback, extra

	#::called by run() and saves queries in self._results::#
	def query_callback(self, answer, qname, rr, flags, extra):
		if answer[0] == adns.status.ok: #::ADD MORE CONDITIONS
			self._results[qname] = ["OK",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.nxdomain:
			self._results[qname] = ["NXDOMAIN",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcodeservfail:
			self._results[qname] = ["SERVFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.prohibitedcname:
			self._results[qname] = ["CNAME is " + answer[1],answer[1],answer[2],answer[3]]
			#self._results[qname] = "CNAME"
			self.send_query(answer[1],adns.rr.A,0,self.query_callback)
		elif answer[0] == adns.status.nodata:
			self._results[qname] = ["NODATA",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.allservfail:
			self._results[qname] = ["ALLSERVFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.answerdomaininvalid:
			self._results[qname] = ["ANSWERDOMAININVALID",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.answerdomaintoolong:
			self._results[qname] = ["ANSWERDOMAINTOOLONG",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.invaliddata:
			self._results[qname] = ["]INVALIDDATA",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.invalidresponse:
			self._results[qname] = ["INVALIDRESPONSE",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_localfail:
			self._results[qname] = ["MAXLOCALFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_misconfig:
			self._results[qname] = ["MAXMISCONFIG",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_misquery:
			self._results[qname] = ["MAXMISQUERY",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_permfail:
			self._results[qname] = ["MAXPERMFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_remotefail:
			self._results[qname] = ["MAXREMOTEFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_tempfail:
			self._results[qname] = ["MAXTEMPFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.nomemory:
			self._results[qname] = ["NOMEMORY",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.norecurse:
			self._results[qname] = ["NORECURSE",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.querydomaininvalid:
			self._results[qname] = ["QUERYDOMAININVALID",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.querydomaintoolong:
			self._results[qname] = ["QUERYDOMAINTOOLONG",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.querydomainwrong:
			self._results[qname] = ["QUERYDOMAINWRONG",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcodeformaterror:
			self._results[qname] = ["RCODEFORMATERROR",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcodenotimplemented:
			self._results[qname] = ["RCODENOTIMPLEMENTED",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcoderefused:
			self._results[qname] = ["RCODEREFUSED",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcodeservfail:
			self._results[qname] = ["RCODESERVFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcodeunknown:
			self._results[qname] = ["RCODEUNKNOWN",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.systemfail:
			self._results[qname] = ["SYSTEMFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.timeout:
			#self._results[qname]=answer[3]
			self._results[qname] = ["TIMEOUT",answer[1],answer[2],answer[3]]
			#self.send_query(qname,adns.rr.A,0,self.query_callback)
		elif answer[0] == adns.status.unknownformat:
			self._results[qname] = ["UNKNOWNFORMAT",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.unknownrrtype:
			self._results[qname] = ["UNKNOWNRRTYPE",answer[1],answer[2],answer[3]]
		else:
			self._results[qname] = ["CATASTROPHIC!!!! CALL THE FEDS",answer[1],answer[2],answer[3]]
			
	def run(self,timeout=0):
		for q in self._s.completed(timeout):
			answer = q.check()
			qname, rr, flags, callback, extra = self._queries[q]
			del self._queries[q]
			apply(callback, (answer, qname, rr, flags, extra))

	def finished(self):
		return not len(self._queries)

	def finish(self):
		while not self.finished():
			self.run(1)

	def resolve(self):
		#log_event("\tBeginning asynchronous resolutions....",False)
		#::Loop through domains sent through self.adns.submit(domain,rr) leveraging callback function::#
		queue=self.hosts[:] #::save us some typing
		for domain in queue:
			self.send_query(domain,self.query_type,0,self.query_callback)
		self.finish()

	def output_results(self,query_source=True,output_mode=False,successful_only=False,filename=False):
		#::mode == output type (file,list,stdout)
		#::successful_only == only print sucessful responses
		#::filename == output file

		#::Process list is used to format results into a proper data structure::#
		#::Reads in self._results, processes it into a master list of lists which:#
		#::is then passed on to other functions when producing output::#

		def process_list():

			def add_to_list(datum):
				tmp_domain=datum[4]
				tmp_response=datum[3]
				tmp_ttl=str(datum[2])
				tmp_cname=datum[1]
				tmp_status=datum[0]

				if not tmp_cname:
					tmp_cname="NO_CNAME"

				#::Different record types will need to be handled differently here as::#
				#::the response field in the list may be of varying data type::#
				if self.query_type == adns.rr.TXT:
					handlemedifferently=True
				else:
					for reply_value in datum[3]:
						append_list=[tmp_status,tmp_domain,tmp_ttl,reply_value,tmp_cname]
						self._final_list.append(append_list)

			#::Begin process_list main()::#
			p_domain = ""
			p_status = ""
			p_ttl = ""
			p_response = ""

			for q_domain,q_response in self._results.items():
				q_response.append(q_domain)
				if successful_only:
					#::Determine if record is a success::#
					if q_response[0] == "OK":
						add_to_list(q_response)
				else:
					if q_response[0] != "OK":
						p_domain = q_domain
						p_cname = "CHECK_STATUS"
						p_status = q_response[0]
						p_ttl = 'CHECK_STATUS'
						p_response = ['CHECK_STATUS']
						quick_list=[p_status,p_cname,p_ttl,p_response,p_domain]
						add_to_list(quick_list)
					else:
						add_to_list(q_response)
		#::This is the one stop shop for generating output, here stdout,file,and list structures are supported::#
		def generate_output():

			if output_mode.upper() == 'TO_FILE':
				output_file=filename
				if filename == False:
					output_file = "APEDNS-" + query_source.upper() + "-" + time.strftime("%Y%m%d%H%M") + ".txt"
				w=open(output_file, 'ab+')
				for query_item in self._final_list:
					w.write(",".join(query_item) + '\n') 
				w.close()
			elif output_mode.upper() == 'TO_LIST':
				return self._final_list
			elif output_mode.upper() == 'STDOUT':
				for query_item in self._final_list:
					print(",".join(query_item))
					
		#::Being our main() equivalent because python sucks!!!!::#
		#::Begin variables::#

		allowed_modes = ['TO_FILE','TO_LIST','STDOUT']

		#::This may seem redundant, but this is planned in the event::#
		#::a user incorrectly calls output_results while this is cron'd::#
		#::the user will not loose data::#

		if filename:
			output_mode='TO_FILE'
		if not output_mode in allowed_modes:
			output_mode='TO_FILE'

		process_list()
		return generate_output()

def batch_mode(args):
	log_event("\t[+] - Entered BATCH MODE",False)
	final_domains=prep_domains(args.mode,args.input)
	ar=apeResolver(domain_list=final_domains,rr_type=args.rrtype)
	start = time.time()
	ar.resolve()
	log_event("\tAsynchronous queries complete, checking for replies....",False)
	end=time.time()
	log_event("[+] - %.2f seconds to resolve %d domains." % (end-start,len(ar._results.keys())),False)
	log_event("\tGenerating output....",False)
	if args.output:
		if args.output.upper() == "STDOUT":
			ar.output_results(query_source=args.mode,output_mode='STDOUT',successful_only=args.output_mode)
		else:
			ar.output_results(query_source=args.mode,output_mode='TO_FILE',successful_only=args.output_mode,filename=args.output)
	else:
		ar.output_results(query_source=args.mode,output_mode='TO_FILE',successful_only=args.output_mode)
	return
	
def brute_mode(args):
    	log_event("[+] - Entered BRUTE-FORCE MODE: (going bananas!!!)",False)  
    	final_domains=prep_domains(args.mode,args.input,args.subdomains) 
	ar=apeResolver(domain_list=final_domains,rr_type=args.rrtype)
	start = time.time()
	ar.resolve()
	log_event("\tAsynchronous queries complete, checking for replies....",False)
	end=time.time()
	log_event("[+] - %.2f seconds to resolve %d domains." % (end-start,len(ar._results.keys())),False)
	log_event("\tGenerating output....",False)
	if args.output:
		if args.output.upper() == "STDOUT":
			ar.output_results(query_source=args.mode,output_mode='STDOUT',successful_only=args.output_mode)
		else:
			ar.output_results(query_source=args.mode,output_mode='TO_FILE',successful_only=args.output_mode,filename=args.output)
	else:
		ar.output_results(query_source=args.mode,output_mode='TO_FILE',successful_only=args.output_mode)
	return

def prep_domains(mode,unval_domains,unval_subdomains=False):
	

	def validate_subdomains(subdomain_list):
		
		def meets_regex(str_check):
			accepted_pattern = re.compile('^[A-Z0-9\-\.]+$',re.IGNORECASE)
			if accepted_pattern.search(str_check):
				return 'GOOD'
			else:
				log_event("\tBAD SUBDOMAIN: %s" % str_check,False)
				return 'BAD'

		if subdomain_list.find(',') != -1:
			log_event("\tSubdomain List Provided: %s" % subdomain_list,False)
			for word in subdomain_list.split(','):
				dict_subdomains[word] = meets_regex(word)
		elif os.path.isfile(subdomain_list):
			log_event("\tSubdomain File List Provided: %s" % subdomain_list,False)
			for word in open(subdomain_list):
				dict_subdomains[word.rstrip('\n')] = meets_regex(word)
		else:
			log_event("\tSubdomain Provided: %s" % subdomain_list,False)
			dict_subdomains[subdomain_list] = meets_regex(subdomain_list)

		num_good=len([subd for subd, value in dict_subdomains.items() if value == 'GOOD'])
		num_bad=len([subd for subd, value in dict_subdomains.items() if value == 'BAD'])

		log_event("\t[-] - GOOD Sub-Domains: %s, BAD Sub-Domains %s" % (num_good,num_bad),False)

	def validate_domains(domain_list):

		def meets_regex(str_check):
			accepted_pattern = re.compile('[A-Z0-9\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|NAME|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW)$', re.IGNORECASE)	
			if accepted_pattern.search(str_check):
				return 'GOOD'
			else:
				log_event("\tBAD DOMAIN PROVIDED: " + str_check,False)
				return 'BAD'

		#::TODO - is there an easier way to just apply a regex to a list/tuple::#
		#::Apply regex rule to domain, stores domain in dictionary dict_domains with a value of GOOD/BAD::#
		if isinstance(domain_list,list):
			log_event("\tDomains provided as list structure: %s" % domain_list,False)
			for word in domain_list:
				dict_domains[word]=meets_regex(word)
		elif os.path.isfile(domain_list):
			log_event("\tDomains provided as file: %s" % domain_list,False)
			for word in open(domain_list):
				dict_domains[word.rstrip('\n')]=meets_regex(word.rstrip('\n'))
		elif domain_list == "stdin":
			log_event("\tDomains provided from stdin: %s" % domain_list,False)
			for word in sys.stdin:
				dict_domains[word.rstrip('\n')]=meets_regex(word.rstrip('\n'))
		else:
			log_event("\t[!] - FATAL ERROR...no domain file.",False)
			sys.exit(0)

		num_good=len([sld for sld, value in dict_domains.items() if value == 'GOOD'])
		num_bad=len([sld for sld, value in dict_domains.items() if value == 'BAD'])

		log_event("\t[-] - GOOD Domains: %s, BAD Domains %s" % (num_good,num_bad),False)

	log_event("\t[-] - Validing domains...",False)
	dict_domains=dict() #::dictionary {'domain':'state good/bad'} - using to generate stats on domain input
	dict_subdomains=dict() #::diciontary {'subdomain':'good/bad'} - stats on subdomain validity
	
	if mode == 'batch':
		validate_domains(unval_domains)
		return [domain for domain, value in dict_domains.items() if value == 'GOOD']

	elif mode == 'brute':
		validate_subdomains(unval_subdomains)
		validate_domains(unval_domains)
		brute_domains_final = []	
		#::Generate list of domains::#
		for subdomain,subdomain_state in dict_subdomains.items():
			if subdomain_state == 'GOOD':
				for domain,domain_state in dict_domains.items():
					if domain_state == 'GOOD':
						brute_domain=subdomain + "." + domain
						brute_domains_final.append(brute_domain)
		return brute_domains_final
		
		#return [subd for subd, value in dict_subdomains.items() if value == 'GOOD']
	else:
		log_event("\t[!] - Something went wrong",False)
		sys.exit(0)

def log_event(msg,show_output):
	f = open("apemessages.log", 'ab+')
	f.write(time.strftime("%Y-%m-%d %H:%M:%S:") + msg + '\n')
	if show_output:
		print msg
	return	
		

def main():

	import os
	import sys
	import argparse

	try:
		import adns
	except:
		print "[!] - FATAL ERROR: adns-python not installed or not working."
		sys.exit(0)

	p = ArgumentParser(
		description='apedns.py - asynchronous dns resolver.',
		usage='apedns.py [MODE] [options]')

	p.add_argument('--rr',action='store',dest='rrtype',choices=['A','NS','PTR'],default='A',help='Record Type',required=False)
	p.add_argument('--successful_only',action='store_true',default=False,dest='output_mode',help='Print successful queries only (default prints OK + failed queries)',required=False)

	sub_args=p.add_subparsers()

	#::Batch mode arguments::#
	sub_batch = sub_args.add_parser('batch',help="Batch resolve a list of predefined domains")
	sub_batch.add_argument('--input',type=str,action='store',dest='input',
				help='stdin/File containing a list of domains to resolve',required=True)
	sub_batch.add_argument('--output',type=str,action='store',dest='output',
				help='stdout/Output file name... default is APEDNSBATCH-YYYYMMDDHHSS.txt',required=False)
	sub_batch.set_defaults(func=batch_mode,mode='batch')
	
	#::brute mode arguments::#
	sub_brute = sub_args.add_parser('brute',help="Brute force a list of domains given a list of predefined domains. (aka: Go bananas.)")
	sub_brute.add_argument('--input',type=str,action='store',dest='input',
				help='stdin/File containing a list of domains resolve subdomains for or comma delimited list of domains',required=True)
	sub_brute.add_argument('--subdomains',action='store',dest='subdomains',
				help='Either comma delimited list of subdomain referents or a file containing subdomain referents.',required=True)
	sub_brute.add_argument('--output',action='store',dest='output',
				help='stdout/Output file name...default is APEDNSBRUTE-YYYMMDDHHSS.txt',required=False)
	sub_brute.set_defaults(func=brute_mode,mode='brute')

	#args = p.parse_args()
	args = p.parse_args(sys.argv[1:])
	args.func(args)
	
if __name__ == '__main__':
	main()
