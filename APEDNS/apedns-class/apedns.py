#!/usr/bin/python

from argparse import ArgumentParser
import argparse
import sys,os
import re
import adns
from time import time
import time
import pprint

class apeResolver(object):

	callback = None
	callback_submit = None

	def __init__(self,domain_list=True,rr_type=True,s=None):
		rr_code = {"A" : 1, "NS" : 2, "PTR" : 65548, "TXT" : 16 }
		self.hosts = domain_list
		self.query_type = rr_code[rr_type]
		self._queries = {} #::dictionary of active query objects, looped through by run()
		self._results = {} #::dictionary of tuples storing query results.
		self._final_list=[] #::This is our master list of lists.
		#self._s = s or adns.init(adns.iflags.noautosys) #::s is custom nameserver configuration
		self._s=adns.init()

	#::submit is called in a loop (resolve()) providing the following arguments::#
	def send_query(self, qname, rr, flags=0, callback=None, extra=None):
		callback = callback or self.callback_submit
		if not callback: raise Error, "callback required"
		q = self._s.submit(qname, rr, flags)
		self._queries[q] = qname, rr, flags, callback, extra

	#::called by run() and saves queries in self._results::#
	def query_callback(self, answer, qname, rr, flags, extra):
		if answer[0] == adns.status.ok: #::ADD MORE CONDITIONS
			self._results[qname] = ["OK",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.nxdomain:
			self._results[qname] = ["NXDOMAIN",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcodeservfail:
			self._results[qname] = ["SERVFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.prohibitedcname:
			self._results[qname] = ["CNAME is " + answer[1],answer[1],answer[2],answer[3]]
			#self._results[qname] = "CNAME"
			self.send_query(answer[1],adns.rr.A,0,self.query_callback)
		elif answer[0] == adns.status.nodata:
			self._results[qname] = ["NODATA",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.allservfail:
			self._results[qname] = ["ALLSERVFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.answerdomaininvalid:
			self._results[qname] = ["ANSWERDOMAININVALID",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.answerdomaintoolong:
			self._results[qname] = ["ANSWERDOMAINTOOLONG",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.invaliddata:
			self._results[qname] = ["]INVALIDDATA",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.invalidresponse:
			self._results[qname] = ["INVALIDRESPONSE",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_localfail:
			self._results[qname] = ["MAXLOCALFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_misconfig:
			self._results[qname] = ["MAXMISCONFIG",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_misquery:
			self._results[qname] = ["MAXMISQUERY",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_permfail:
			self._results[qname] = ["MAXPERMFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_remotefail:
			self._results[qname] = ["MAXREMOTEFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.max_tempfail:
			self._results[qname] = ["MAXTEMPFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.nomemory:
			self._results[qname] = ["NOMEMORY",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.norecurse:
			self._results[qname] = ["NORECURSE",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.querydomaininvalid:
			self._results[qname] = ["QUERYDOMAININVALID",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.querydomaintoolong:
			self._results[qname] = ["QUERYDOMAINTOOLONG",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.querydomainwrong:
			self._results[qname] = ["QUERYDOMAINWRONG",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcodeformaterror:
			self._results[qname] = ["RCODEFORMATERROR",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcodenotimplemented:
			self._results[qname] = ["RCODENOTIMPLEMENTED",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcoderefused:
			self._results[qname] = ["RCODEREFUSED",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcodeservfail:
			self._results[qname] = ["RCODESERVFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.rcodeunknown:
			self._results[qname] = ["RCODEUNKNOWN",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.systemfail:
			self._results[qname] = ["SYSTEMFAIL",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.timeout:
			#self._results[qname]=answer[3]
			self._results[qname] = ["TIMEOUT",answer[1],answer[2],answer[3]]
			#self.send_query(qname,adns.rr.A,0,self.query_callback)
		elif answer[0] == adns.status.unknownformat:
			self._results[qname] = ["UNKNOWNFORMAT",answer[1],answer[2],answer[3]]
		elif answer[0] == adns.status.unknownrrtype:
			self._results[qname] = ["UNKNOWNRRTYPE",answer[1],answer[2],answer[3]]
		else:
			self._results[qname] = ["CATASTROPHIC!!!! CALL THE FEDS",answer[1],answer[2],answer[3]]
			
	def run(self,timeout=0):
		for q in self._s.completed(timeout):
			answer = q.check()
			qname, rr, flags, callback, extra = self._queries[q]
			del self._queries[q]
			apply(callback, (answer, qname, rr, flags, extra))

	def finished(self):
		return not len(self._queries)

	def finish(self):
		while not self.finished():
			self.run(1)

	def resolve(self):
		#log_event("\tBeginning asynchronous resolutions....",False)
		#::Loop through domains sent through self.adns.submit(domain,rr) leveraging callback function::#
		queue=self.hosts[:] #::save us some typing
		for domain in queue:
			self.send_query(domain,self.query_type,0,self.query_callback)
		self.finish()

	def output_results(self,query_source=True,output_mode=False,successful_only=False,filename=False):
		#::mode == output type (file,list,stdout)
		#::successful_only == only print sucessful responses
		#::filename == output file

		#::Process list is used to format results into a proper data structure::#
		#::Reads in self._results, processes it into a master list of lists which:#
		#::is then passed on to other functions when producing output::#

		def process_list():

			def add_to_list(datum):
				tmp_domain=datum[4]
				tmp_response=datum[3]
				tmp_ttl=str(datum[2])
				tmp_cname=datum[1]
				tmp_status=datum[0]

				if not tmp_cname:
					tmp_cname="NO_CNAME"

				#::Different record types will need to be handled differently here as::#
				#::the response field in the list may be of varying data type::#
				if self.query_type == adns.rr.TXT:
					handlemedifferently=True
				else:
					for reply_value in datum[3]:
						append_list=[tmp_status,tmp_domain,tmp_ttl,reply_value,tmp_cname]
						self._final_list.append(append_list)

			#::Begin process_list main()::#
			p_domain = ""
			p_status = ""
			p_ttl = ""
			p_response = ""

			for q_domain,q_response in self._results.items():
				q_response.append(q_domain)
				if successful_only:
					#::Determine if record is a success::#
					if q_response[0] == "OK":
						add_to_list(q_response)
				else:
					if q_response[0] != "OK":
						p_domain = q_domain
						p_cname = "CHECK_STATUS"
						p_status = q_response[0]
						p_ttl = 'CHECK_STATUS'
						p_response = ['CHECK_STATUS']
						quick_list=[p_status,p_cname,p_ttl,p_response,p_domain]
						add_to_list(quick_list)
					else:
						add_to_list(q_response)
		#::This is the one stop shop for generating output, here stdout,file,and list structures are supported::#
		def generate_output():

			if output_mode.upper() == 'TO_FILE':
				output_file=filename
				if filename == False:
					output_file = "APEDNS-" + query_source.upper() + "-" + time.strftime("%Y%m%d%H%M") + ".txt"
				w=open(output_file, 'ab+')
				for query_item in self._final_list:
					w.write(",".join(query_item) + '\n') 
				w.close()
			elif output_mode.upper() == 'TO_LIST':
				return self._final_list
			elif output_mode.upper() == 'STDOUT':
				for query_item in self._final_list:
					print(",".join(query_item))
					
		#::Being our main() equivalent because python sucks!!!!::#
		#::Begin variables::#

		allowed_modes = ['TO_FILE','TO_LIST','STDOUT']

		#::This may seem redundant, but this is planned in the event::#
		#::a user incorrectly calls output_results while this is cron'd::#
		#::the user will not loose data::#

		if filename:
			output_mode='TO_FILE'
		if not output_mode in allowed_modes:
			output_mode='TO_FILE'

		process_list()
		return generate_output()
