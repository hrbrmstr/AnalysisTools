#!/usr/bin/python

from argparse import ArgumentParser
import argparse
import sys,os
import re
import adns
from time import time
import time

class apeResolver(object):

	callback = None
	callback_submit = None

	def __init__(self,hosts,s=None):
		self.hosts = hosts
		self._queries = {} #::dictionary of active query objects, looped through by run()
		self._results = {} #::dictionary of tuples storing query results.
		#self._s = s or adns.init(adns.iflags.noautosys) #::s is custom nameserver configuration
		self._s=adns.init()

	#::submit is called in a loop (resolve()) providing the following arguments::#
	def send_query(self, qname, rr, flags=0, callback=None, extra=None):
		callback = callback or self.callback_submit
		if not callback: raise Error, "callback required"
		q = self._s.submit(qname, rr, flags)
		self._queries[q] = qname, rr, flags, callback, extra

	#::called by run() and saves queries in self._results::#
	def query_callback(self, answer, qname, rr, flags, extra):
		if answer[0] == adns.status.ok: #::ADD MORE CONDITIONS
			self._results[qname] = answer[3]
		elif answer[0] == adns.status.nxdomain:
			self._results[qname] = "NXDOMAIN"
		elif answer[0] == adns.status.rcodeservfail:
			self._results[qname] = "SERVFAIL"
		elif answer[0] == adns.status.prohibitedcname:
			#self._results[qname] = "CNAME"
			self.send_query(answer[1],adns.rr.A,0,self.query_callback)
		elif answer[0] == adns.status.nodata:
			self._results[qname] = "NODATA"
		elif answer[0] == adns.status.allservfail:
			self._results[qname] = "ALLSERVFAIL"
		elif answer[0] == adns.status.answerdomaininvalid:
			self._results[qname] = "ANSWERDOMAININVALID"
		elif answer[0] == adns.status.answerdomaintoolong:
			self._results[qname] = "ANSWERDOMAINTOOLONG"
		elif answer[0] == adns.status.invaliddata:
			self._results[qname] = "INVALIDDATA"
		elif answer[0] == adns.status.invalidresponse:
			self._results[qname] = "INVALIDRESPONSE"
		elif answer[0] == adns.status.max_localfail:
			self._results[qname] = "MAXLOCALFAIL"
		elif answer[0] == adns.status.max_misconfig:
			self._results[qname] = "MAXMISCONFIG"
		elif answer[0] == adns.status.max_misquery:
			self._results[qname] = "MAXMISQUERY"
		elif answer[0] == adns.status.max_permfail:
			self._results[qname] = "MAXPERMFAIL"
		elif answer[0] == adns.status.max_remotefail:
			self._results[qname] = "MAXREMOTEFAIL"
		elif answer[0] == adns.status.max_tempfail:
			self._results[qname] = "MAXTEMPFAIL"
		elif answer[0] == adns.status.nomemory:
			self._results[qname] = "NOMEMORY"
		elif answer[0] == adns.status.norecurse:
			self._results[qname] = "NORECURSE"
		elif answer[0] == adns.status.querydomaininvalid:
			self._results[qname] = "QUERYDOMAININVALID"
		elif answer[0] == adns.status.querydomaintoolong:
			self._results[qname] = "QUERYDOMAINTOOLONG"
		elif answer[0] == adns.status.querydomainwrong:
			self._results[qname] = "QUERYDOMAINWRONG"
		elif answer[0] == adns.status.rcodeformaterror:
			self._results[qname] = "RCODEFORMATERROR"
		elif answer[0] == adns.status.rcodenotimplemented:
			self._results[qname] = "RCODENOTIMPLEMENTED"
		elif answer[0] == adns.status.rcoderefused:
			self._results[qname] = "RCODEREFUSED"
		elif answer[0] == adns.status.rcodeservfail:
			self._results[qname] = "RCODESERVFAIL"
		elif answer[0] == adns.status.rcodeunknown:
			self._results[qname] = "RCODEUNKNOWN"
		elif answer[0] == adns.status.systemfail:
			self._results[qname] = "SYSTEMFAIL"
		elif answer[0] == adns.status.timeout:
			#self._results[qname]=answer[3]
			self._results[qname] = "TIMEOUT"
			#self.send_query(qname,adns.rr.A,0,self.query_callback)
		elif answer[0] == adns.status.unknownformat:
			self._results[qname] = "UNKNOWNFORMAT"
		elif answer[0] == adns.status.unknownrrtype:
			self._results[qname] = "UNKNOWNRRTYPE"
		else:
			self._results[qname] = "CATASTROPHIC!!!! CALL THE FEDS"
			
	def run(self,timeout=0):
		for q in self._s.completed(timeout):
			answer = q.check()
			qname, rr, flags, callback, extra = self._queries[q]
			del self._queries[q]
			apply(callback, (answer, qname, rr, flags, extra))

	def finished(self):
		return not len(self._queries)

	def finish(self):
		while not self.finished():
			self.run(1)

	def resolve(self):
		log_event("\tBeginning asynchronous resolutions....",False)
		#::Loop through domains sent through self.adns.submit(domain,rr) leveraging callback function::#
		queue=self.hosts[:] #::save us some typing
		for domain in queue:
			self.send_query(domain,adns.rr.A,0,self.query_callback)

def brute_mode(args):
	#::Constants::#
	log_event("[+] - Entered BRUTE-FORCE MODE: (going bananas!!!)",True)		
	final_domains=prep_domains(args.mode,args.input,args.subdomains) 
	ar=apeResolver(final_domains)
	start = time.time()
	ar.resolve()
	log_event("\tAsynchronous queries complete, checking for replies....",False)
	ar.finish()
	resolved=ar._results
	end=time.time()
	log_event("[+] - %.2f seconds to resolve %d domains." % (end-start,len(ar._results.keys())),True)
	log_event("\tGenerating output....",False)
	gen_output(resolved,args.mode,args.output)
	return

#::The following function is temporarily defunct::#
def gen_output(resolved_data,mode,output_arg=False):
        output=""
        #::If no --output switch, save to file::#
        if not output_arg:
                output= "APDNS-" + mode.upper() + "-" + time.strftime("%Y%m%d%H%M") + ".txt"
        elif output_arg == "stdout":
                output="stdout"
        else:
                output=output_arg
    
    	#::TODO - This can obviously be better represented::#
	log_event("\t[-] - Saving output to: %s" % output,False)
        if output != "stdout":
		g=open(output, 'ab+')
		for key,value in resolved_data.items():
			if isinstance(value,tuple):
				for ip in value:
					g.write(str(key) + "," + str(ip) + '\n')
			else:
				g.write(str(key) + "," + str(value) + '\n')
        	g.close()
        else:
        	for key,value in resolved_data.items():
			if isinstance(value,tuple):
				for ip in value:
					print(str(key) + "," + str(ip))
			else:
				print(str(key) + "," + str(value))

        return


def prep_domains(mode,unval_domains,unval_subdomains=False):
	

	def validate_subdomains(subdomain_list):
		
		def meets_regex(str_check):
			accepted_pattern = re.compile('^[A-Z0-9\-\.]+$',re.IGNORECASE)
			if accepted_pattern.search(str_check):
				return 'GOOD'
			else:
				log_event("\tBAD SUBDOMAIN: %s" % str_check,False)
				return 'BAD'

		if subdomain_list.find(',') != -1:
			log_event("\tSubdomain List Provided: %s" % subdomain_list,False)
			for word in subdomain_list.split(','):
				dict_subdomains[word] = meets_regex(word)
		elif os.path.isfile(subdomain_list):
			log_event("\tSubdomain File List Provided: %s" % subdomain_list,False)
			for word in open(subdomain_list):
				dict_subdomains[word.rstrip('\n')] = meets_regex(word)
		else:
			log_event("\tSubdomain Provided: %s" % subdomain_list,False)
			dict_subdomains[subdomain_list] = meets_regex(subdomain_list)

		num_good=len([subd for subd, value in dict_subdomains.items() if value == 'GOOD'])
		num_bad=len([subd for subd, value in dict_subdomains.items() if value == 'BAD'])

		log_event("\t[-] - GOOD Sub-Domains: %s, BAD Sub-Domains %s" % (num_good,num_bad),True)

	def validate_domains(domain_list):

		def meets_regex(str_check):
			accepted_pattern = re.compile('[A-Z0-9\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|NAME|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW)$', re.IGNORECASE)	
			if accepted_pattern.search(str_check):
				return 'GOOD'
			else:
				log_event("\tBAD DOMAIN PROVIDED: " + str_check,False)
				return 'BAD'

		#::TODO - is there an easier way to just apply a regex to a list/tuple::#
		#::Apply regex rule to domain, stores domain in dictionary dict_domains with a value of GOOD/BAD::#
		if isinstance(domain_list,list):
			log_event("\tDomains provided as list structure: %s" % domain_list,False)
			for word in domain_list:
				dict_domains[word]=meets_regex(word)
		elif os.path.isfile(domain_list):
			log_event("\tDomains provided as file: %s" % domain_list,False)
			for word in open(domain_list):
				dict_domains[word.rstrip('\n')]=meets_regex(word.rstrip('\n'))
		elif domain_list == "stdin":
			log_event("\tDomains provided from stdin: %s" % domain_list,False)
			for word in sys.stdin:
				dict_domains[word.rstrip('\n')]=meets_regex(word.rstrip('\n'))
		else:
			log_event("\t[!] - FATAL ERROR...no domain file.",True)
			sys.exit(0)

		num_good=len([sld for sld, value in dict_domains.items() if value == 'GOOD'])
		num_bad=len([sld for sld, value in dict_domains.items() if value == 'BAD'])

		log_event("\t[-] - GOOD Domains: %s, BAD Domains %s" % (num_good,num_bad),True)

	log_event("\t[-] - Validing domains...",True)
	dict_domains=dict() #::dictionary {'domain':'state good/bad'} - using to generate stats on domain input
	dict_subdomains=dict() #::diciontary {'subdomain':'good/bad'} - stats on subdomain validity
	
	if mode == 'batch':
		validate_domains(unval_domains)
		return [domain for domain, value in dict_domains.items() if value == 'GOOD']

	elif mode == 'brute':
		validate_subdomains(unval_subdomains)
		validate_domains(unval_domains)
		brute_domains_final = []	
		#::Generate list of domains::#
		for subdomain,subdomain_state in dict_subdomains.items():
			if subdomain_state == 'GOOD':
				for domain,domain_state in dict_domains.items():
					if domain_state == 'GOOD':
						brute_domain=subdomain + "." + domain
						brute_domains_final.append(brute_domain)
		return brute_domains_final
		
		#return [subd for subd, value in dict_subdomains.items() if value == 'GOOD']
	else:
		log_event("\t[!] - Something went wrong",True)
		sys.exit(0)	

def log_event(msg,show_output):
	f = open("apemessages.log", 'ab+')
	f.write(time.strftime("%Y-%m-%d %H:%M:%S:") + msg + '\n')
	if show_output:
		print msg
	return	
		

def main():

	import os
	import sys
	import argparse

	try:
		import adns
	except:
		print "[!] - FATAL ERROR: adns-python not installed or not working."
		sys.exit(0)
	
	p = ArgumentParser(
		description='apedns-brute.py - asynchronous dns resolver used to identify subdomains. Outputs resolution results in comma delimited format (CSV).',
		usage='apedns.py --input --subdomains --output')
	
	#::brute mode arguments::#
	p.add_argument('--input',type=str,action='store',dest='input',
				help='stdin/File containing a list of domains resolve subdomains for or comma delimited list of domains',required=True)
	p.add_argument('--subdomains',action='store',dest='subdomains',
				help='Either comma delimited list of subdomain referents or a file containing subdomain referents.',required=True)
	p.add_argument('--output',action='store',dest='output',
				help='stdout/Output file name...default is APEDNSBRUTE-YYYMMDDHHSS.txt',required=False)
	p.set_defaults(func=brute_mode,mode='brute')

	#args = p.parse_args()
	args = p.parse_args(sys.argv[1:])
	args.func(args)
	
if __name__ == '__main__':
	main()
