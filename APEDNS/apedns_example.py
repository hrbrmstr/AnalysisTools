#!/usr/bin/python

import os,sys
from apedns import *

#::CONSTRUCTORS/METHODS::#
#::	----
#::	apeResolver() - builds a apedns resolver object
#::	Expects: 
#::		domain_list - list object of domains.
#::		rr_type - RR Type (A|NS|PTR) - more to be supported soon
#::	Returns:
#::		apeResolver object.
#:: 	----
#::	resolve() - forces resolution - flow continues when all queries receive responses and/or timeout.
#::
#::	----
#::	output_results() - outputs results to STDOUT,TO_FILE,TO_LIST
#::	Expects:
#::		query_source - name of your tracker/script
#::		output_mode - (TO_FILE|TO_LIST|STDOUT)
#::
#::		successful_only - (True|False) - used to distinguish between outputting successful queries or successful + failed queries.
#::		filename - Optional: this is used as a filename for TO_FILE, if not provided while using TO_FILE, a default output file is created.
#::			NOTE: filename serves as an override. If provided with any other output mode, the script will default to output_mode: TO_FILE
#::	----
#::	Other notes: if a bogus rr_type is provided, it defaults to "A"

def resolve_stdout(domain_as_list):
	ar=apeResolver(domain_list=domain_as_list,rr_type="A")
	ar.resolve()
	ar.output_results(query_source="MY_DAILY_TRACKER",output_mode='STDOUT',successful_only=False)
	return

def resolve_to_file(domain_as_list):
	ar=apeResolver(domain_list=domain_as_list,rr_type="A")
	ar.resolve()
	ar.output_results(query_source="MY_DAILY_TRACKER",output_mode='TO_FILE',successful_only=False,filename="myfilename.txt")
	return

def resolve_to_list(domains_as_list):
	ar=apeResolver(domain_list=domain_as_list,rr_type="A")
	ar.resolve()
	ape_query_results=ar.output_results(query_source="MY_DAILY_TRACKER",output_mode='TO_LIST',successful_only=False)
	
	for result in ape_query_results:
		print result[0] #STATUS CODE
		print result[1] #domain
		print result[2] #TTL
		print result[3] #response data
		print result[4] #cname (if any)
	return

myDomains = ['google.com','mike-matonis.com']

#resolve_stdout(myDomains)
#resolve_to_file(myDomains)
#resolve_to_list(myDomains)
