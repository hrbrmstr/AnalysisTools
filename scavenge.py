#!/usr/bin/python

"""
Used to collect info about an address, or group of addresses, across multiple sources.

Current checks include:
    VirusTotal (malware results & pDNS)
    Hurricane Electric
    ISC
    Farsight pDNS
    ipinfo.io
    IOCDB
    IPVoid / URLVoid

Usage Examples:
    scavenge.py 127.0.0.1 -o filename.txt <- Single IP lookup w/ output to a file
    scavenge.py bulk -i input.txt -o      <- Bulk IP lookup w/ output to a file
"""

import argparse
import csv
import datetime
import json
import os
import os.path
import re
import subprocess
import sys
import unicodedata
import urllib
import urllib2
import ConfigParser
from netaddr import IPAddress, IPNetwork
from BeautifulSoup import BeautifulSoup, BeautifulStoneSoup

# Global variable containing configuration values
config = ConfigParser.ConfigParser()
global DEBUG
DEBUG = True


# Utility function to get a URL with error handling
# Accepts URL string or urllib2.Request object
def get_url(orig_request):
    if isinstance(orig_request, basestring):
        url = orig_request.encode('utf8')
        request = urllib2.Request(url)
    elif isinstance(orig_request, urllib2.Request):
        request = orig_request
    else:
        return None

    try:
        response = urllib2.urlopen(request)
    except urllib2.HTTPError as e:
        sys.stderr.write("The server couldn't fulfill the request for URL %s: %s\n" % (request.get_full_url(), e))
        return None
    except urllib2.URLError as e:
        sys.stderr.write('We failed to reach a server for URL %s: %s\n' % (request.get_full_url(), e))
        return None
    else:
        return response


# Checks VirusTotal for occurrences of an IP address
def vt_ip_check(ip, vt_api):
    try:
        if DEBUG:
            sys.stderr.write("Attempting VT retrieval for %s\n" % ip)
        url = 'https://www.virustotal.com/vtapi/v2/ip-address/report'
        parameters = {'ip': ip, 'apikey': vt_api}
        # TODO: use get_url() to get error handling and similar benefits
        response = urllib.urlopen('%s?%s' % (url, urllib.urlencode(parameters))).read()
        return_dict = json.loads(response)
        return return_dict
    except:
        return None


# Checks VirusTotal for occurrences of a domain name
def vt_name_check(domain, vt_api):
    try:
        if DEBUG:
            sys.stderr.write("Attempting VT retrieval for %s\n" % domain)
        url = 'https://www.virustotal.com/vtapi/v2/domain/report'
        parameters = {'domain': domain, 'apikey': vt_api}
        response = urllib.urlopen('%s?%s' % (url, urllib.urlencode(parameters))).read()
        return_dict = json.loads(response)
        return return_dict
    except:
        return None


# Checks Hurricane Electric for DNS information on an IP address
def he_ip_check(ip):
    if DEBUG:
        sys.stderr.write("Attempting HE retrieval for %s\n" % ip)
    request = urllib2.Request('http://bgp.he.net/ip/' + ip + '#_dns')
    request.add_header('User-Agent',
                       'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36')
    response = get_url(request)
    if response:
        pattern = re.compile('\/dns\/.+\".title\=\".+\"\>(.+)<\/a\>', re.IGNORECASE)
        hostnames = re.findall(pattern, str(response.read()))
        return hostnames
    else:
        return None


# Checks Hurricane Electric for DNS information on an IP address
def he_name_check(domain):
    if DEBUG:
        sys.stderr.write("Attempting HE retrieval for %s\n" % domain)
    request = urllib2.Request('http://bgp.he.net/dns/' + domain + '#_whois')
    request.add_header('User-Agent',
                       'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36')
    response = get_url(request)
    if response:
        # TODO: get good sample to review
        pattern = re.compile('\/dns\/.+\".title\=\".+\"\>(.+)<\/a\>', re.IGNORECASE)
        hostnames = re.findall(pattern, str(response.read()))
        return hostnames
    else:
        return None


# Checks SANS ISC for attack data on an IP address
def isc_ip_check(ip):
    try:
        if DEBUG:
            sys.stderr.write("Attempting ISC retrieval for %s\n" % ip)
        request = urllib2.Request('https://isc.sans.edu/api/ip/' + ip)
        request.add_header('User-Agent',
                           'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36')
        response = get_url(request)
        data = BeautifulStoneSoup(response.read())
        return {'count': data.count.text,
                'attacks': data.attacks.text,
                'mindate': data.mindate.text,
                'maxdate': data.maxdate.text}
    except:
        return None


# Checks Farsight passive DNS for information on an IP address
def pdns_ip_check(ip, dnsdb_api):
    pdns_results = []
    url = 'https://api.dnsdb.info/lookup/rdata/ip/' + ip + '?limit=50'

    request = urllib2.Request(url)
    request.add_header('Accept', 'application/json')
    request.add_header('X-Api-Key', dnsdb_api)

    if DEBUG:
        sys.stderr.write("Attempting pDNS retrieval for %s\n" % ip)
    response = get_url(request)
    if not response:
        return False
    pdns_json_all = response.read()
    if len(pdns_json_all) > 0:
        for pdns_json in pdns_json_all.rstrip('\n').split('\n'):
            try:
                answer = json.loads(pdns_json)
                if 'rrname' in answer:
                    pdns_results.append(answer)
            except:
                sys.stderr.write("Could not parse '%s' as JSON\n" % pdns_json)
                pass
        return pdns_results
    else:
        return None


# Checks Farsight passive DNS for information on an IP address
def pdns_name_check(name, dnsdb_api):
    pdns_results = []
    url = 'https://api.dnsdb.info/lookup/rrset/name/' + name + '?limit=50'

    request = urllib2.Request(url)
    request.add_header('Accept', 'application/json')
    request.add_header('X-Api-Key', dnsdb_api)

    if DEBUG:
        sys.stderr.write("Attempting pDNS retrieval for %s\n" % name)
    response = get_url(request)
    if not response:
        return False
    pdns_json_all = response.read()
    if len(pdns_json_all) > 0:
        for pdns_json in pdns_json_all.rstrip('\n').split('\n'):
            try:
                answer = json.loads(pdns_json)
                if 'rrname' in answer:
                    pdns_results.append(answer)
            except:
                sys.stderr.write("Could not parse '%s' as JSON\n" % pdns_json)
                pass
        return pdns_results
    else:
        return None


# Checks IOCDB for rumors about a set of addresses
def iocdb_all_check(address, iocdb_info, infile):
    outdir = '/tmp'
    outfile = "iocdb-" + str(os.getpid()) + ".json"
    (user, server, port) = iocdb_info

    if DEBUG:
        sys.stderr.write("Attempting IOCDB retrieval for %s\n" % address)
    if infile is not None:
        cmd = '/usr/local/bin/iocdb --encoder json --observable_file ~/' + infile + ' > ' + outfile
        try:
            subprocess.call(['scp', infile, 'query:/home/' + user])
        except:
            sys.stderr.write('Unable to reach %s@%s:%s' % (user, server, port))
            return False
    else:
        cmd = '/usr/local/bin/iocdb rumors --encoder json --observable_values %s > %s' % (address, outfile)

    try:
        subprocess.call(['ssh', 'query', cmd])
        subprocess.call(['scp', 'query:/home/' + user + '/' + outfile, outdir])
    except:
        sys.stderr.write('Unable to reach %s@%s:%s' % (user, server, port))
        return False

    sys.stderr.write("Original IOCDB JSON output in %s\n" % os.path.join(outdir, outfile))

    with open(os.path.join(outdir, outfile), 'r') as data:
        ioc_data = data.read()
        try:
            return json.loads(ioc_data)
        except:
            return None


# Checks ipinfo.io for basic WHOIS-type data on an IP address
def ipinfo_ip_check(ip):
    if DEBUG:
        sys.stderr.write("Attempting IPinfo retrieval for %s\n" % ip)

    response = get_url('http://ipinfo.io/' + ip + '/json')
    if response:
        try:
            result = json.load(response)
            return result
        except:
            return False
    else:
        return None


def ipvoid_check(ip):
    if not re.match('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', ip):
        return None

    if DEBUG:
        sys.stderr.write("Attempting IPVoid retrieval for %s\n" % ip)

    return_dict = dict()
    url = 'http://ipvoid.com/scan/%s/' % ip
    response = get_url(url)
    data = BeautifulSoup(response.read())
    if data.findAll('span', attrs={'class': 'label label-success'}):
        return None
    elif data.findAll('span', attrs={'class': 'label label-danger'}):
        for each in data.findAll('img', alt='Alert'):
            detect_site = each.parent.parent.td.text.lstrip()
            detect_url = each.parent.a['href']
            return_dict[detect_site] = detect_url
    else:
        if DEBUG:
            sys.stderr.write('Could not find IPVoid decision for %s' % ip)
        return None

    if len(return_dict) == 0:
        return None
    return return_dict


def urlvoid_check(name):
    if not re.match('[\.a-zA-Z]', name):
        return None

    if DEBUG:
        sys.stderr.write("Attempting URLVoid retrieval for %s\n" % name)

    return_dict = dict()
    url = 'http://urlvoid.com/scan/%s/' % name
    response = get_url(url)
    data = BeautifulSoup(response.read())
    if data.findAll('div', attrs={'class': 'bs-callout bs-callout-info'}):
        return None
    elif data.findAll('div', attrs={'class': 'bs-callout bs-callout-warning'}):
        for each in data.findAll('img', alt='Alert'):
            detect_site = each.parent.parent.td.text.lstrip()
            detect_url = each.parent.a['href']
            return_dict[detect_site] = detect_url
    else:
        if DEBUG:
            sys.stderr.write('Could not find URLVoid decision for %s\n' % name)
        return None

    if len(return_dict) == 0:
        return None
    return return_dict


def urlvoid_ip_check(ip):
    if not re.match('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', ip):
        return None

    if DEBUG:
        sys.stderr.write("Attempting URLVoid retrieval for %s\n" % ip)

    return_dict = dict()
    url = 'http://urlvoid.com/ip/%s/' % ip
    response = get_url(url)
    data = BeautifulSoup(response.read())
    h1 = data.findAll('h1')[0].text
    if h1 == 'Report not found':
        return None
    elif re.match('^IP', h1):
        return_dict['bad_names'] = []
        return_dict['other_names'] = []
        for each in data.findAll('img', alt='Alert'):
            return_dict['bad_names'].append(
                each.parent.a.get('href').rstrip('/').replace('http://www.urlvoid.com/scan/', ''))
        for each in data.findAll('img', alt='Valid'):
            return_dict['other_names'].append(
                each.parent.a.get('href').rstrip('/').replace('http://www.urlvoid.com/scan/', ''))
    else:
        if DEBUG:
            sys.stderr.write('Could not find URLVoid decision for %s\n' % ip)
        return None

    if len(return_dict) == 0:
        return None
    return return_dict


def print_data(data):
    print "# %s" % data['address']

    if 'whitelist' in data:
        print '\n## Address whitelisted as %s' % data['whitelist']

    if 'network_data' in data:
        print "\n## IP Info"
        if 'hostname' in data['network_data']:
            print "- Hostname: %s" % data['network_data']['hostname']
        if 'org' in data['network_data']:
            print "- Org: %s" % data['network_data']['org']
        if 'city' in data['network_data'] and 'country' in data['network_data'] and 'region' in data['network_data']:
            for each in data['network_data']:
                if data['network_data'][each]:
                    data['network_data'][each] = unicodedata.normalize('NFKD', data['network_data'][each]).encode('ascii',
                                                                                                                  'ignore')
            print "- Location: %s, %s, %s" % (data['network_data']['city'],
                                              data['network_data']['country'],
                                              data['network_data']['region'])

    if 'hostnames' in data:
        print "\n## Hostnames (via Hurricane Electric)"
        for hostname in data['hostnames']:
            print "- %s" % hostname

    if 'pdns' in data:
        print "\n## Passive DNS (via DNSDB)"
        # TODO: maybe reformat as table?
        for pdns_rec in data['pdns']:
            if data['type'] == 'ip':
                if 'time_first' in pdns_rec and 'time_last' in pdns_rec:
                    pfirst = datetime.datetime.fromtimestamp(int(pdns_rec['time_first'])).strftime("%Y-%m-%d")
                    plast = datetime.datetime.fromtimestamp(int(pdns_rec['time_last'])).strftime("%Y-%m-%d")
                    print "- Hostname: %s Count: %d First: %s Last: %s" % (pdns_rec['rrname'],
                                                                           pdns_rec['count'],
                                                                           pfirst, plast)
                elif 'zone_time_first' in pdns_rec and 'zone_time_last' in pdns_rec:
                    pfirst = datetime.datetime.fromtimestamp(int(pdns_rec['zone_time_first'])).strftime("%Y-%m-%d")
                    plast = datetime.datetime.fromtimestamp(int(pdns_rec['zone_time_last'])).strftime("%Y-%m-%d")
                    print "- Hostname: %s Count: %d First: %s Last: %s" % (pdns_rec['rrname'],
                                                                           pdns_rec['count'],
                                                                           pfirst, plast)
                else:
                    print "- Hostname: %s Count: %d " % (pdns_rec['rrname'], pdns_rec['count'])
            else:
                if 'time_first' in pdns_rec and 'time_last' in pdns_rec:
                    pfirst = datetime.datetime.fromtimestamp(int(pdns_rec['time_first'])).strftime("%Y-%m-%d")
                    plast = datetime.datetime.fromtimestamp(int(pdns_rec['time_last'])).strftime("%Y-%m-%d")
                    # TODO: This should be processed as a list of addresses? Need example data
                    print "- Address: %s Count: %d First: %s Last: %s" % (pdns_rec['rdata'][0],
                                                                          pdns_rec['count'],
                                                                          pfirst, plast)
                elif 'zone_time_first' in pdns_rec and 'zone_time_last' in pdns_rec:
                    pfirst = datetime.datetime.fromtimestamp(int(pdns_rec['zone_time_first'])).strftime("%Y-%m-%d")
                    plast = datetime.datetime.fromtimestamp(int(pdns_rec['zone_time_last'])).strftime("%Y-%m-%d")
                    print "- Address: %s Count: %d First: %s Last: %s" % (pdns_rec['rdata'][0],
                                                                          pdns_rec['count'],
                                                                          pfirst, plast)
                else:
                    print "- Address: %s Count: %d " % (pdns_rec['rdata'][0], pdns_rec['count'])

    if 'isc' in data:
        print "\n## ISC Results"
        print "- Total count: %s" % data['isc']['count']
        print "- Number of attacks: %s" % data['isc']['attacks']
        print "- First seen: %s" % data['isc']['mindate']
        print "- Last seen: %s" % data['isc']['maxdate']

    if 'virustotal' in data:
        print "\n## VirusTotal Results"
        if 'response_code' in data['virustotal'] and data['virustotal']['response_code'] == 0:
            print "- **%s**" % data['virustotal']['verbose_msg']
        else:
            if 'categories' in data['virustotal']:
                print "\n### Categories:"
                for c in data['virustotal']['categories']:
                    print "- %s" % c
            if 'resolutions' in data['virustotal']:
                print "\n### DNS Resolutions"
                for resolution in data['virustotal']['resolutions']:
                    if 'hostname' in resolution:
                        print "- %s %s" % (resolution['hostname'], resolution['last_resolved'])
                    elif 'ip_address' in resolution:
                        print "- %s %s" % (resolution['ip_address'], resolution['last_resolved'])
            if 'detected_urls' in data['virustotal']:
                print "\n### Detected URLs"
                for detect in data['virustotal']['detected_urls']:
                    print "- %s Positives: %d Scan Date: %s" % (detect['url'], detect['positives'], detect['scan_date'])
            if 'detected_downloaded_samples' in data['virustotal']:
                print "\n### Detected Downloaded Samples"
                for detect in data['virustotal']['detected_downloaded_samples']:
                    print "- %s (count: %d) at %s" % (detect['sha256'], detect['positives'], detect['date'])

    if 'ipvoid' in data:
        print "\n## IPVoid Results"
        for each in data['ipvoid']:
            print "- %s: see %s" % (each, data['ipvoid'][each])

    if 'urlvoid_names' in data:
        print "\n## URLVoid Results"
        print "\n### Alert names"
        for each in data['urlvoid_names']['bad_names']:
            print "- %s" % each
        print "\n### Other names"
        for each in data['urlvoid_names']['other_names']:
            print "- %s" % each
    elif 'urlvoid' in data:
        print "\n## URLVoid Results"
        for each in data['urlvoid']:
            print "- %s: see %s" % (each, data['urlvoid'][each])

    if 'rumors' in data:
        print "\n## IOCDB Results"
        for rumor in data['rumors']:
            print "- %s via %s on %s (TLP:%s)" % (rumor['description'], rumor['document']['source'],
                                                  rumor['document']['received'], rumor['document']['tlp'])
            if rumor['document']['nocomm']:
                print "    - **NOCOMM**"
            if rumor['document']['noforn']:
                print "    - **NOFORN**"

    print "\n---\n"


def main():
    parser = argparse.ArgumentParser(description="Conduct multiple IP lookups to compile a comprehensive report")
    parser.add_argument('address', nargs=1,
                        help='The address (IP, domain, etc.) to Scavenge, use "bulk" for a file-based lookup')
    parser.add_argument('-i', '--input', help='Conduct a bulk lookup from file')
    parser.add_argument('-o', '--output', help='Specify an output file for the report')
    parser.add_argument('-s', '--save', help='Save raw JSON separately', action='store_true')
    args = parser.parse_args()

    config.read('analysis.cfg')
    vt_api = config.get('ScavengeIP', 'vt_api')
    dnsdb_api = config.get('ScavengeIP', 'dnsdb_api')
    iocdb_info = (config.get('ScavengeIP', 'iocdb_user'),
                  config.get('ScavengeIP', 'iocdb_server'),
                  config.get('ScavengeIP', 'iocdb_port'))
    wl_file = config.get('ScavengeIP', 'whitelist_file')

    whitelist = [{'net': IPNetwork('10.0.0.0/8'), 'org': 'Private per RFC 1918'},
                 {'net': IPNetwork('172.16.0.0/12'), 'org': 'Private per RFC 1918'},
                 {'net': IPNetwork('192.168.0.0/16'), 'org': 'Private per RFC 1918'},
                 {'net': IPNetwork('0.0.0.0/8'), 'org': 'Invalid per RFC 1122'},
                 {'net': IPNetwork('127.0.0.0/8'), 'org': 'Loopback per RFC 1122'},
                 {'net': IPNetwork('169.254.0.0/16'), 'org': 'Link-local per RFC 3927'},
                 {'net': IPNetwork('100.64.0.0/10'), 'org': 'Shared address space per RFC 6598'},
                 {'net': IPNetwork('192.0.0.0/24'), 'org': 'IETF Protocol Assignments per RFC 6890'},
                 {'net': IPNetwork('192.0.2.0/24'), 'org': 'Documentation and examples per RFC 6890'},
                 {'net': IPNetwork('192.88.99.0/24'), 'org': 'IPv6 to IPv4 relay per RFC 3068'},
                 {'net': IPNetwork('198.18.0.0/15'), 'org': 'Network benchmark tests per RFC 2544'},
                 {'net': IPNetwork('198.51.100.0/24'), 'org': 'Documentation and examples per RFC 5737'},
                 {'net': IPNetwork('203.0.113.0/24'), 'org': 'Documentation and examples per RFC 5737'},
                 {'net': IPNetwork('224.0.0.0/4'), 'org': 'IP multicast per RFC 5771'},
                 {'net': IPNetwork('240.0.0.0/4'), 'org': 'Reserved per RFC 1700'},
                 {'net': IPNetwork('255.255.255.255/32'), 'org': 'Broadcast address per RFC 919'}, ]
    if wl_file:
        sys.stderr.write('Reading whitelist from %s\n' % wl_file)
        with open(wl_file, 'r') as f:
            for row in csv.reader(f):
                whitelist.append({'net': IPNetwork(row[0]), 'org': row[1]})

    if args.output:
        oldout = sys.stdout
        sys.stdout = open(args.output, 'w')

    if args.address[0] == 'bulk':
        if not args.input:
            raise Exception("Please specify an input file using the -i switch")
        with open(args.input, 'r') as in_file:
            addresses = [address.strip() for address in in_file]
    else:
        addresses = [args.address[0]]

    scavenged_data = dict()

    for address in addresses:
        if re.match('\s*', address):
            pass
        elif address == '':
            pass
        scavenged_data[address] = dict()
        scavenged_data[address]['address'] = address
        if re.match('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', address):
            scavenged_data[address]['type'] = 'ip'
            if whitelist:
                for wl_net in whitelist:
                    if IPAddress(address) in wl_net['net']:
                        scavenged_data[address]['whitelist'] = wl_net['org']
            if 'whitelist' not in scavenged_data[address]:
                scavenged_data[address]['network_data'] = ipinfo_ip_check(address)
                scavenged_data[address]['hostnames'] = he_ip_check(address)
                if vt_api:
                    # TODO: find a more Pythonic way to do this
                    vt_data = vt_ip_check(address, vt_api)
                    if vt_data:
                        scavenged_data[address]['virustotal'] = vt_data
                isc_data = isc_ip_check(address)
                if isc_data:
                    scavenged_data[address]['isc'] = isc_data
                if dnsdb_api:
                    pdns_data = pdns_ip_check(address, dnsdb_api)
                    if pdns_data:
                        scavenged_data[address]['pdns'] = pdns_data
                ipvoid_data = ipvoid_check(address)
                if ipvoid_data:
                    scavenged_data[address]['ipvoid'] = ipvoid_data
                urlvoid_data = urlvoid_ip_check(address)
                if urlvoid_data:
                    scavenged_data[address]['urlvoid_names'] = urlvoid_data
        else:
            scavenged_data[address]['type'] = 'name'
            scavenged_data[address]['hostnames'] = he_name_check(address)
            if vt_api:
                vt_data = vt_name_check(address, vt_api)
                if vt_data:
                    scavenged_data[address]['virustotal'] = vt_data
            if dnsdb_api:
                pdns_data = pdns_name_check(address, dnsdb_api)
                if pdns_data:
                    scavenged_data[address]['pdns'] = pdns_data
            urlvoid_data = urlvoid_check(address)
            if urlvoid_data:
                scavenged_data[address]['urlvoid'] = urlvoid_data
    if len(iocdb_info) == 3:
        iocdb_results = iocdb_all_check(args.address[0], iocdb_info, args.input)
        if iocdb_results:
            for rumor in iocdb_results:
                address = rumor['observable']['value']
                if 'rumors' not in scavenged_data[address]:
                    scavenged_data[address]['rumors'] = []
                scavenged_data[address]['rumors'].append(rumor)

    for address in addresses:
        print_data(scavenged_data[address])

    if args.save:
        with open(os.path.join('/tmp', 'scavenge-' + str(os.getpid()) + '.json'), 'w') as out_file:
            json.dump(scavenged_data, out_file, sort_keys=True, indent=4, separators=(',', ': '))
            sys.stderr.write("Dumped raw JSON in /tmp/scavenge-%s.json\n\n" % str(os.getpid()))

    if args.output:
        sys.stdout.close()
        sys.stdout = oldout


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        sys.exit()
