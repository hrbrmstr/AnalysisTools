#!/usr/bin/env python

import os,sys
import csv
import netaddr
import re
from argparse import ArgumentParser

def print_range(org,start,end):
	ip_list=list(netaddr.iter_iprange(start,end))
	for ip in ip_list:
		print "%s,%s" % (ip,org)

def short2long(ip):
	short_ip=int(ip)
	oct1=short_ip/(256 ** 3)
	oct2=((short_ip%(256 ** 3)/(256 ** 2)))
	oct3=(short_ip%(256 **3))%(256 **2)/(256**1)
	oct4=(short_ip%(256**3))%(256**2)%(256**1)/(256**0)

	dotted_ip="%d.%d.%d.%d" % (oct1,oct2,oct3,oct4)

	return dotted_ip

def main():

	p = ArgumentParser(description='pyWHITELIST.py',usage='figure it out.')
	p.add_argument('--maxmind',action='store',dest='maxmind',help='Maxmind Org CSV',required=True)
	p.add_argument('--orgs',action='store',dest='organizations',help='List of Organizational Identifiers',required=True)
	p.add_argument('--exclude',action='store',dest='exclude',help='Organizational keywords to exclude',required=False)
	p.add_argument('--print-range',action='store_true',default=False,dest='range_print',help='Print all IP addresses in range',required=False)

	args = p.parse_args(sys.argv[1:])

	#::List of organizations::#
	org_list=args.organizations.split(",")
	range_cache=dict()

	#::Read in CSV::#
	maxmind_db=open(args.maxmind,'rb')
	reader=csv.reader(maxmind_db)
	for line in reader:
		organization=line[2]
		#::Match org identifiers::#
		for org in org_list:
			orgex=re.compile(org,re.IGNORECASE)
			if orgex.search(organization):
				#::Exclude keywords::#
				if args.exclude:
					excl_list=args.exclude.split(",")
					found=False
					for i in range(len(excl_list)):
						exorgex=re.compile(excl_list[i],re.IGNORECASE)
						if not exorgex.search(organization) and found == False:
							if i == len(excl_list)-1:
								start_ip=short2long(line[0])
								end_ip=short2long(line[1])

								range_cache[(start_ip,end_ip)] = organization
						else:
							found=True
				else:
					start_ip=short2long(line[0])
					end_ip=short2long(line[1])
					range_cache[(start_ip,end_ip)] = organization

	if args.range_print == False:
		for netrange,org in range_cache.items():	
			print "%s,%s,%s" % (netrange[0],netrange[1],org)
	else:
		for netrange,org in range_cache.items():	
			print_range(org,netrange[0],netrange[1])

	maxmind_db.close()



if __name__ == '__main__':
	main()
