vzFLOW
=======
**BETA** Verizon netflow analysis utility. Produces reports summarizing communication characteristics of analysis IP addresses (protocols, inbound, outbound, data exchange figures, and recurrance).

##Requirements:
* **Install Maxmind databases** Follow procedure for Analysis Tools/pyGEO
 
##Arguments:
* **--file=** Flow data csv from NI
* **--ips=** Comma delimited list of IP addresses or CIDR netblocks input to NI
* **--tag=** Label for analysis session, used to label output files in VZFLOW-[tag]-[REPORT].txt format. 

##Usage Example (MASK Indicators):
```
$ python ~/Desktop/vzFLOW.py --file=results.csv --ips=200.46.107.115,210.48.153.236,82.103.129.239,202.75.56.123,202.75.56.231,202.75.58.153,200.85.152.61 --tag="MASK-RESEARCH"

	vzFLOW.py - find what's upstream.

                   |  `'.
__           |`-._/_.:---`-.._
\='.       _/..--'`__         `'-._
 \- '-.--"`      ===        /   o  `',
  )= (                 .--_ |       _.'
 /_=.'-._             {=_-_ |   .--`-.
/_.'    `\`'-._        '-=   \    _.'
    jgs  )  _.-'`'-..       _..-'`
        /_.'        `/";';`|
                     \` .'/
                      '--'

		
[+] - Successfully opened: results.csv
[+] - Loading GeoIP Data...
[+] - Populating analysis IP addresses...
[+] - Beginning analysis...
[+] - Generating reports...

$ ls -l
-rw-r--r--  1 analyst  staff   3630 Feb 19 16:25 VZFLOW-MASK-RESEARCH-BYTESBYPAIR.csv
-rw-r--r--  1 analyst  staff    160 Feb 19 16:25 VZFLOW-MASK-RESEARCH-COUNTRY.csv
-rw-r--r--  1 analyst  staff   9488 Feb 19 16:25 VZFLOW-MASK-RESEARCH-GEOIP.csv
-rw-r--r--  1 analyst  staff   1237 Feb 19 16:25 VZFLOW-MASK-RESEARCH-OUTBOUND.csv
-rw-r--r--  1 analyst  staff   5183 Feb 19 16:25 VZFLOW-MASK-RESEARCH-SUMMARY.txt
-rwxr-xr-x  1 analyst  staff  92841 Feb 12 14:57 results.csv

```
##Reports Created:
In the current working directory, the following reports are created:
* **SUMMARY** - summary of protocol statistics, countries, ASNs, inbound/outbound communications, byte exchanges, intersections, and hourly/daily recurrance.
* **BYTESBYPAIR** - list of analysis IP and external IP address total byte exchanges.
* **COUNTRY** - list of countries seen and number of unique IP addresses
* **GEOIP** - Geolocation dump of all observed external IP addresses
* **OUTBOUND** - list of outbound communications, organizational details, and flow count.

##Roadmap:
* Summary reporting based on popular protocols
* IP level reports similar to overall SUMMARY report
* GUI (bottle.py & d3js & sqlite or MySQL for report caching and analysis techniques)
* Automatic mapping of protocols to service names (i.e. 80 = HTTP)
* Better handling of directional reporting based on popular protocols with ephemeral port mappings (e.g. VoIP, RDP, etc.)
