#!/usr/bin/env python

import os,sys
import re
import pprint
import time
import datetime
import GeoIP
import re
import collections
import json
from argparse import ArgumentParser
from netaddr import *

class vzFLOW(object):

	#def __init__(self,flow_data,ip_addresses,html_out=False,session_name):
	def __init__(self,flow_data,ip_addresses,session_name):

		self.ascii_art()

		try:
			self._flow = open(flow_data,'rt')
		except IOError:
			print "[!] - Couldn't open %s ... exiting" % (flow_data)
			sys.exit(0)

		print "[+] - Successfully opened: %s" % (flow_data)

		self._session_name=session_name
		self._flow_file=flow_data

		self._analysis_ip_addresses = ip_addresses
		self._analysis_ips = dict() #::Dict for analysis IPs : Key - IP, Value - List of Geolocation Data
		self._external_ips = dict() #::Dict for external IPs (format same as analysis)
		self._day_by_ip = dict()
		self._weekday_by_ip = collections.defaultdict(dict)
		self._external_ip_hours = dict() #::Dict, used for beaconing / C2 detection
		self._ips = dict() #::Dict for IP addresses::#
		self._destination_ports = collections.defaultdict(int) #::Dict for destination ports::#
		self._bytes_by_ip = collections.defaultdict(int) #::Dict for total bytes by IP::#
		self._bytes_by_pair = collections.defaultdict(int) #::Dict for total bytes by internal->external
		self._bytes_range_by_ip = dict() #::Dict for range of IPs
		self._flow_by_day = collections.defaultdict(int) #::Dict counting flows by day::#
		self._flow_by_weekday = [0,0,0,0,0,0,0]
		self._intersection_count = collections.defaultdict(int) #::Dict for source IP->Dest IP (used for intersection) ::#
		self._maxmind = dict() #::Key - IP, Value - List of values::#
		self._protocol_sum = collections.defaultdict(int)
		self._protocol_by_ip = dict()
		self._ip_distinct_days = dict() #::How many days has the destination IP been seen::#
		self._anomalies = dict()
		self._outbound_comms = dict()
		self._country_count=collections.defaultdict(int)
		self._asn_count=collections.defaultdict(int)
		self._organization_count=collections.defaultdict(int)

		#::quick hacks::#
		self._hour_summary = dict()
		self._outbound_count = dict()
		self._outbound_frequency = collections.defaultdict(int)
		self._outbound_intersection = collections.defaultdict(int)

		print "[+] - Loading GeoIP Data..."
		#::GeoIP Files::#
		self._geoIP_city = GeoIP.open("/usr/local/share/GeoIP/GeoLiteCity.dat",GeoIP.GEOIP_MEMORY_CACHE)
		self._geoIP_country = GeoIP.open("/usr/local/share/GeoIP/GeoIP.dat",GeoIP.GEOIP_MEMORY_CACHE)
		self._geoIP_asn = GeoIP.open("/usr/local/share/GeoIP/GeoIPASNum.dat",GeoIP.GEOIP_MEMORY_CACHE)
		self._geoIP_organization = GeoIP.open("/usr/local/share/GeoIP/GeoIPOrg.dat",GeoIP.GEOIP_MEMORY_CACHE)

		#::Populate source IPs::#
		print "[+] - Populating analysis IP addresses..."
		self._network_list = self._analysis_ip_addresses.split(",")
		for network in self._network_list:
			cidr_notation = "/"
			if cidr_notation in network:
				tmp_ip = IPNetwork(network)
				for ip in tmp_ip:
					self._analysis_ips[str(ip)] = self.maxmind_return(ip)
			else:
					self._analysis_ips[str(network)] = self.maxmind_return(network)

		print '[+] - Beginning analysis...'
		self.analyze_flow()

		#print "[+] - Building graphs..."

		#self.json_proto()

		print '[+] - Generating reports...'
		self.report_overview()
		self.report_summations()

	def maxmind_return(self,ip_addy): 
		#::City Data::#
		city = self._geoIP_city.record_by_addr(str(ip_addy))
		if city == None:
			self._asn_count['NONROUTE'] +=1
			self._country_count['NONROUTE'] +=1
			self._organization_count['NONROUTE'] +=1
			return ['NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE']
		
		for key,value in city.items():
			if city[key] == None:
				city[key] = "NULL"

		#::ASN::#
		asn = self._geoIP_asn.name_by_addr(str(ip_addy))
		asn_work=""
		asn_id=""
		asn_org=""
		if asn == None:
			asn_work="NULL"
			asn_id="NULL"
			asn_org="NULL"
		else:
			asn_work = asn.split(" ")
			asn_id = asn_work[0]
			asn_org = " ".join(asn_work[1:])
		
		#::Organization::#
		organization = self._geoIP_organization.org_by_addr(str(ip_addy))
		if organization == None:
			organization="NULL"
		org_netmask = self._geoIP_organization.last_netmask()

		#:: [0] - organization, [1] - asn_id, [2] - asn_org, [3] - org_netmask, [4] country_code, [5] - country_name, [6] - CITY, [7] STATE, [8] REGION, [9] - REGION_NAME, [10] - TIMEZONE
		geolocated_list = [organization.strip(','),str(asn_id),asn_org,str(org_netmask),city['country_code'].strip(','),city['country_name'].strip(','),city['city'].strip(','),city['region'].strip(','),city['region_name'].strip(','),city['time_zone'].strip(',')]

		#::geo stats::#
		self._asn_count[asn_id] +=1
		self._country_count[city['country_name']] +=1
		self._organization_count[organization] +=1

		return geolocated_list

	def json_proto(self):
		json_data=[]
		for key,value in self._protocol_sum.items():
			json_data.append({'label' : key, 'value' : value})

		print json.dumps(json_data)

	def analyze_flow(self):
		
		for flow in self._flow:
			if not flow.startswith('#'):
				flow_items=flow.rstrip().split(',')
				start_time = flow_items[0]
				date_time = start_time.split(' ')
				end_time = flow_items[1]
				duration = flow_items[2]
				src = flow_items[3]
				dst = flow_items[4]
				proto = flow_items[5]
				src_port = flow_items[6]
				dst_port = flow_items[7]
				tcp_flags = flow_items[8]
				packets = flow_items[9]
				bytes = flow_items[10]
				sensor = flow_items[11]
				in_if = flow_items[12]
				out_if = flow_items[13]
				log_time = flow_items[14]
				src_asn = flow_items[15]
				dst_asn = flow_items[16]
				source_file = flow_items[17]

				#::ANALYSIS BEGINS HERE::#
				if not (self._analysis_ips.has_key(src) and self._analysis_ips.has_key(dst)):
					self.flow_by_day(date_time[0])
					self.protocol_sum(proto)

					if self._analysis_ips.has_key(src):
						self.sends_external(src,dst,int(src_port),int(dst_port),True)
						if not self._external_ips.has_key(dst):
							self.external_geo(dst)
						self.byte_by_ip(src,dst,bytes)
						self.byte_by_pair(src,dst,bytes)
						self.external_ip_hourly(dst,start_time)
						self.external_ip_proto(dst,proto)
						self.days_ip_seen(dst,date_time[0])
					else:
						self.sends_external(src,dst,int(src_port),int(dst_port),False)
						if not self._external_ips.has_key(src):
							self.external_geo(src)
						self.byte_by_ip(dst,src,bytes)
						self.byte_by_pair(dst,src,bytes)
						self.external_ip_hourly(src,start_time)
						self.external_ip_proto(src,proto)
						self.days_ip_seen(src,date_time[0])


		#::Build overall stats::#
		#:-day -> weekday:#
		for date,count in self._flow_by_day.items():
			date_break = date.split('-')
			self._flow_by_weekday[int(datetime.date(int(date_break[0]),int(date_break[1]),int(date_break[2])).weekday())] += 1

		#::IP by weekday & Count of distinct days::#
		for ip,date in self._day_by_ip.items():
			#::Count of distinct days::#
			self._ip_distinct_days[ip] = len(date)
			for days,count in date.items():
				date_break = days.split('-')
				weekday=int(datetime.date(int(date_break[0]),int(date_break[1]),int(date_break[2])).weekday())
				if self._weekday_by_ip[ip].has_key(weekday):
					self._weekday_by_ip[ip][weekday] +=1
				else:
					self._weekday_by_ip[ip][weekday] = {}
					self._weekday_by_ip[ip][weekday] = 1

		#::Intersection for time period::#
		for key in self._bytes_by_pair.items():
			self._intersection_count[key[0][1]] += 1

		#::Summing Hour count::#
		for tmpIP,hours in self._external_ip_hours.items():
			self._hour_summary[tmpIP] = len(hours)

		#::Distinct outbound IP addresses::#
		for tmpIP,tmpDST in self._outbound_comms.items():
			self._outbound_count[tmpIP] = len(tmpDST)
			#::intersection::#
			for subIP,value in tmpDST.items():
				self._outbound_intersection[subIP] +=1

		#for key,value in self._outbound_intersection.items():
		#	print "%s - %s" % (key,value)

	def report_summations(self):

		#::GeoIP::#
		w_geoIP = open('VZFLOW-' + self._session_name + "-GEOIP.csv",'wb+')
		for tmpIP,geo in self._external_ips.items():
			w_geoIP.write(tmpIP + "," + ",".join(geo) + '\n')
		w_geoIP.close()

		#::Country Dump::#
		w_Country = open('VZFLOW-' + self._session_name + "-COUNTRY.csv",'wb+')
		for country,count in self._country_count.items():
			w_Country.write(country + "," + str(count) + '\n')
		w_Country.close()

		#::Outbound Dump::#
		w_Outbound = open('VZFLOW-' + self._session_name + "-OUTBOUND.csv",'wb+')
		for tmpIP,count in self._outbound_frequency.items():
			w_Outbound.write(tmpIP + "," + self._external_ips[tmpIP][0] + "," + str(count) + '\n')
		w_Outbound.close()

		#::Bytes by Pair::#
		w_BytePair = open('VZFLOW-' + self._session_name + "-BYTESBYPAIR.csv",'wb+')
		for tmpIP,count in self._bytes_by_pair.items():
			w_BytePair.write(tmpIP[0] + "," + tmpIP[1] + "," + self._external_ips[tmpIP[1]][0] + "," + str(count) + '\n')
		w_BytePair.close()


	def report_overview(self):
		overall_report = open('VZFLOW-' + self._session_name + "-SUMMARY.txt",'wb+')
		#::Header::#
		header="""\
#############################
# vzFLOW.py 
# Session: %s
# Generated: %s
# Source File: %s
# Analysis IPs: %s
#############################\n""" % (self._session_name,time.strftime("%c"),self._flow_file,self._analysis_ip_addresses)

		overall_report.write(header + '\n')
		general="""\
\n#::GENERAL STATS:
-------------------"""
		overall_report.write(general + '\n')
		overall_report.write("Total Unique IP External IP addresses seen: %s" % (len(self._bytes_by_ip)) + '\n')
		#overall_report.write("First flow seen: %s " % (sorted(self._flow_by_day, key=self._flow_by_day.get, reverse=True)[0]) + '\n')
		#overall_report.write("Last flow seen: %s " % (sorted(self._flow_by_day, key=self._flow_by_day.get, reverse=True)[len(self._flow_by_day)-1]) + '\n')

		countries="""\
\n#::COUNTRY STATS:
-------------------"""
		overall_report.write(countries+ '\n')
		overall_report.write("Total Countries: %s" % (len(self._country_count))+ '\n')
		overall_report.write("\nTop 10 Countries (Unique IP per Country): "+ '\n')
		for country in sorted(self._country_count, key=self._country_count.get, reverse=True)[:10]:
			overall_report.write("\t-%s: %s"  % (country,self._country_count[country])+ '\n')

		networks="""\
\n#::NETWORK STATS:
-------------------"""
		overall_report.write(networks+ '\n')
		overall_report.write("ASN Count: %s" % (len(self._asn_count))+ '\n')
		overall_report.write("\nTop 10 ASNs (Unique IP per ASN):"+ '\n')
		for asn in sorted(self._asn_count, key=self._asn_count.get, reverse=True)[:10]:
			overall_report.write("\t-%s: %s"  % (asn,self._asn_count[asn])+ '\n')

		header_orgs="""\
\n#::ORGANIZATIONAL STATS:
-------------------"""
		overall_report.write(header_orgs+ '\n')
		overall_report.write("Total Organizations by IP: %s" % (len(self._organization_count))+ '\n')
		overall_report.write("\nTop 10 Organizations (Unique IP per Organization):"+ '\n')
		for organization in sorted(self._organization_count, key=self._organization_count.get, reverse=True)[:10]:
			overall_report.write("\t-%s: %s"  % (organization,self._organization_count[organization])+ '\n')

		header_protocol="""\
\n#::PROTOCOL STATS:
-------------------"""
		overall_report.write(header_protocol+ '\n')
		overall_report.write("IP Protocol Breakdown (Flow Count per Protocol):"+ '\n')
		for protocol in sorted(self._protocol_sum, key=self._protocol_sum.get, reverse=True):
			overall_report.write("\t-%s: %s"  % (protocol,self._protocol_sum[protocol])+ '\n')

		overall_report.write("\nTop 10 Transport Protocols (Flow Count per Protocol):"+ '\n')
		for tmpProto in sorted(self._destination_ports, key=self._destination_ports.get, reverse=True)[:10]:
			overall_report.write("\t-%s: %s" % (tmpProto,self._destination_ports[tmpProto]) + '\n')

		header_outbound="""\
\n#::OUTBOUND COMMUNICATIONS:
-------------------"""
		overall_report.write(header_outbound+ '\n')
		overall_report.write("The following IP addresses initiated outbound TCP communications to # distinct IPs:"+ '\n')
		for tmpIP,count in self._outbound_count.items():
			overall_report.write("\t-%s: %s"  % (tmpIP,self._outbound_count[tmpIP])+ '\n')

		overall_report.write("\nTop 10 outbound IP addresses:"+ '\n')
		for tmpIP in sorted(self._outbound_frequency, key=self._outbound_frequency.get, reverse=True)[:10]:
			overall_report.write("\t-%s (%s): %s"  % (tmpIP,self._external_ips[tmpIP][0],self._outbound_frequency[tmpIP])+ '\n')

		overall_report.write("\nOutbound intersections (Top 10):"+ '\n')
		for tmpIP in sorted(self._outbound_intersection, key=self._outbound_intersection.get, reverse=True):
			if self._outbound_intersection[tmpIP] > 1:
				overall_report.write("\t-%s (%s): %s"  % (tmpIP,self._external_ips[tmpIP][0],self._outbound_intersection[tmpIP])+ '\n')

		header_data="""\
\n#::DATA EXCHANGE STATS:
-------------------"""
		overall_report.write(header_data+ '\n')
		overall_report.write("Top 10 Total Transfers (bytes) by External IP:"+ '\n')
		for byteIP in sorted(self._bytes_by_ip, key=self._bytes_by_ip.get, reverse=True)[:10]:
			overall_report.write("\t-%s (%s): %s"  % (byteIP,self._external_ips[byteIP][0],self._bytes_by_ip[byteIP])+ '\n')
		overall_report.write("\nTop 10 Total Byte Transfers by IP Pair (Analysis IP->External):"+ '\n')
		for bytePair in sorted(self._bytes_by_pair, key=self._bytes_by_pair.get, reverse=True)[:10]:
			overall_report.write("\t-%s -> %s (%s): %s"  % (bytePair[0],bytePair[1],self._external_ips[bytePair[1]][0],self._bytes_by_pair[bytePair])+ '\n')

		header_recurring="""\
\n#::FREQUENTLY RECURRING IPs BY DAY COUNT:
-------------------"""
		overall_report.write(header_recurring+ '\n')
		overall_report.write("Top 10 Frequently Recurring IP Addresses (# days seen):"+ '\n')
		for tmpIP in sorted(self._ip_distinct_days, key=self._ip_distinct_days.get, reverse=True)[:10]:
				overall_report.write("\t-%s (%s): %s"  % (tmpIP,self._external_ips[tmpIP][0],self._ip_distinct_days[tmpIP])+ '\n')

		header_C2="""\
\n#::FREQUENTLY RECURRING IPs BY HOUR (Cheap C2/Victim Identification):
-------------------"""
		overall_report.write(header_C2+ '\n')
		overall_report.write("Top 30 IP addresses seen communicating by hour (# of distinct hours seen):"+ '\n')
		for tmpIP in sorted(self._hour_summary, key=self._hour_summary.get, reverse=True)[:30]:
				overall_report.write("\t-%s (%s): %s"  % (tmpIP,self._external_ips[tmpIP][0],self._hour_summary[tmpIP])+ '\n')

		header_intersections="""\
\n#::IP INTERSECTIONS:
-------------------"""
		overall_report.write(header_intersections+ '\n')
		overall_report.write("List of external IP addresses seen by > 1 analysis IP:"+ '\n')
		for tmpIP,count in self._intersection_count.items():
			if count > 1:
				overall_report.write("\t-%s (%s): %s"  % (tmpIP,self._external_ips[tmpIP][0],count)+ '\n')

		overall_report.close()

	def sends_external(self,src,dst,src_port,dst_port,is_src):

		def update_list(internal_ip,outbound_ip,dport):

			#::Cheap way to update external connection::#
			self._outbound_frequency[outbound_ip] += 1

			if self._outbound_comms.has_key(internal_ip):
				try:
					self._outbound_comms[internal_ip][outbound_ip] += 1
				except:
					self._outbound_comms[internal_ip][outbound_ip] = 1
			else:
				self._outbound_comms[internal_ip] = dict()
				self._outbound_comms[internal_ip][outbound_ip] = 1

		#::TODO - more robust inbound/outbound protocol figurations::#
		self._destination_ports[src_port] += 1
		self._destination_ports[dst_port] += 1

		if is_src:
			if src_port > 1024:
				update_list(src,dst,dst_port)
		else:
			if dst_port > 1024:
				update_list(dst,src,src_port)

	def days_ip_seen(self,external,date):
		
		if self._day_by_ip.has_key(external):
			try:
				self._day_by_ip[external][date] += 1
			except:
				self._day_by_ip[external][date] = 1
		else:
			self._day_by_ip[external] = {}
			self._day_by_ip[external][date] = 1

	def external_ip_proto(self,external,proto):

		if self._protocol_by_ip.has_key(external):
			try:
				self._protocol_by_ip[external][proto] += 1
			except:
				self._protocol_by_ip[external][proto] = 1
		else:
			self._protocol_by_ip[external] = {}
			self._protocol_by_ip[external][proto] = 1

	def protocol_sum(self,proto):

		self._protocol_sum[proto]+=1

	def flow_by_day(self,date):

		self._flow_by_day[date]+=1

	def external_ip_hourly(self,external,start_time):
		#::Function maps distinct hours where communications to an IP are seen. May be useful for beaconing activities::#
		timez=start_time.split(" ")
		time_break=timez[1].split(":")
		hour=time_break[0]

		#::TODO: This can be better::#
		if self._external_ip_hours.has_key(external):
			try:
				self._external_ip_hours[external][hour]+=1
			except:
				self._external_ip_hours[external][hour] = 1
		else:
			self._external_ip_hours[external] = {}
			self._external_ip_hours[external][hour] = 1

	def byte_by_pair(self,internal,external,bytes):
		#::byte_by_tuple::#
		#try:
		self._bytes_by_pair[(internal,external)]+=int(bytes)
		#except:
		#	self._bytes_by_pair[(internal,external)]=int(bytes)

	def byte_by_ip(self,internal,external,bytes):
		#::Function accumulates bytes by IP as a whole and by an src->dst tuple::#

		#::byte_by_ip::#
		#try:
		self._bytes_by_ip[external]+=int(bytes)
		#except:
			#self._bytes_by_ip[external]=int(bytes)

	def external_geo(self,ext_ip):
		self._external_ips[ext_ip] = self.maxmind_return(ext_ip)

	def ascii_art(self):
		flow_phish="""\

	vzFLOW.py - find what's upstream.

                   |  `'.
__           |`-._/_.:---`-.._
\='.       _/..--'`__         `'-._
 \- '-.--"`      ===        /   o  `',
  )= (                 .--_ |       _.'
 /_=.'-._             {=_-_ |   .--`-.
/_.'    `\`'-._        '-=   \    _.'
    jgs  )  _.-'`'-..       _..-'`
        /_.'        `/";';`|
                     \` .'/
                      '--'

		"""

		print flow_phish

def main():
	p = ArgumentParser(description='vzFLOW.py - script used to do baseline analysis on netflow',usage='vzFLOW.py --file=[file] --analysis=[*,*] --html_report=[name]')
	p.add_argument('--file',action='store',dest='flow_file',help='File to Parse',required=True)
	p.add_argument('--ips',action='store',dest='ip_addresses',help='File to Parse',required=True)
	#p.add_argument('--analysis',action='store',dest='analysis',choices=['all','inbound','outbound','servers'],default='all',help='Analysis Type',required=False)
	p.add_argument('--tag',action='store',dest='session_name',help='Session Name',required=True)
	args = p.parse_args(sys.argv[1:])

	flow=vzFLOW(args.flow_file,args.ip_addresses,args.session_name)

if __name__ == '__main__':
        main()
