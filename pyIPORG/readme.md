pyIPORG
=======
Dump a list of IP addresses owned by an organization given IP whois as tracked by MaxMind. (IP or netranges)

##Usage
```
# python pyWHITELIST.py --maxmind=GeoIPOrg.csv --orgs=Akamai,Google
1.0.0.0,1.0.0.255,Google
1.1.1.0,1.1.1.255,Google
1.2.3.0,1.2.3.255,Google
2.16.0.0,2.16.3.255,Akamai Technologies
2.16.6.0,2.16.33.255,Akamai Technologies

# python pyWHITELIST.py --maxmind=GeoIPOrg.csv --orgs=Akamai,Google --print-range | head -5
1.0.0.0,Google
1.0.0.1,Google
1.0.0.2,Google
1.0.0.3,Google
1.0.0.4,Google

```
* **You can also exclude organizational identifiers by using --exclude**
