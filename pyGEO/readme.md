pyGEO
=======
Python geolocation based on the MaxMind database. pyGEO reads in the first argument and will determine if the argument is an IP, network (CIDR notation), or file and will resolve as appropriate.

##Installation
* **Install Geo DBs** Copy to: /usr/local/share/GeoIP/
* **Install GeoIP C Library** http://www.maxmind.com/download/geoip/api/c/GeoIP-latest.tar.gz
* **Install GeoIP Python Package** https://github.com/maxmind/geoip-api-python

##Usage
```
# python pyGEO.py 42.61.2.55
42.61.2.55,/17,SG,Singapore,Singapore Telecommunications Ltd,42.61.0.0-42.61.127.255,AS3758,Singapore,00,NULL,Asia/Singapore

# python pyGEO.py 8.8.8.8/31
8.8.8.8,/24,US,United States,Google,8.8.8.0-8.8.8.255,AS15169,NULL,NULL,NULL,NULL
8.8.8.9,/24,US,United States,Google,8.8.8.0-8.8.8.255,AS15169,NULL,NULL,NULL,NULL

```
