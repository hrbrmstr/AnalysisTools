#!/usr/bin/python

import os,sys
import GeoIP
import unicodedata
from netaddr import *
import csv

def list_ips(cidr_net):
	ip_addresses=[]
	ip_cidr=IPNetwork(cidr_net)
	for i in range(len(ip_cidr)):
		ip_addresses.append(ip_cidr[i])

	return ip_addresses

def as_file(ip_file):
	
	with open(ip_file) as f:
		for line in f:
			instance=line.rstrip()
			if "/" in instance:
				ips=list_ips(str(instance))
				for ip in ips:
					geo_locate(str(ip))
			else:
				geo_locate(line.rstrip())

def geo_locate(ip_addy):

	city = geoIP_city.record_by_addr(str(ip_addy))
	if city == None:
		print ",".join([str(ip_addy),'NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE','NONROUTE'])
		return
	
	for key,value in city.items():
		if value == None:
			city[key] = "NULL"
	#::ASN::#
	asn = geoIP_asn.name_by_addr(str(ip_addy))
	asn_work=""
	asn_id=""
	asn_org=""
	if asn == None:
		asn_work="NULL"
		asn_id="NULL"
		asn_org="NULL"
	else:
		asn_work = asn.split(" ")
		asn_id = asn_work[0]
		asn_org = " ".join(asn_work[1:])
		
	#::Organization::#
	organization = geoIP_organization.org_by_addr(str(ip_addy))
	if organization == None:
		organization="NULL"
	org_netmask = geoIP_organization.last_netmask()
	org_range="-".join(geoIP_organization.range_by_ip(str(ip_addy)))

	#:: [0] - organization, [1] - asn_id, [2] - asn_org, [3] - org_netmask, [4] country_code, [5] - country_name, [6] - CITY, [7] STATE, [8] REGION, [9] - REGION_NAME, [10] - TIMEZONE
	geolocated_list = [str(ip_addy),"/" + str(org_netmask),city['country_code'],city['country_name'],organization,org_range,str(asn_id),city['city'],city['region'],city['region_name'],city['time_zone']]
	for i in range(len(geolocated_list)):
		if isinstance(geolocated_list[i], unicode):
			geolocated_list[i] = unicodedata.normalize('NKFD',geolocated_list[i])
	csv_write=csv.writer(sys.stdout,delimiter=',',quotechar='"',quoting=csv.QUOTE_ALL)
	for i in range(len(geolocated_list)):
		geolocated_list[i]=geolocated_list[i].decode('unicode_escape').encode('ascii','ignore')
	#csv_write((geolocated_list).decode('unicode_escape').encode('ascii','ignore'))
	csv_write.writerow(geolocated_list)

IP=sys.argv[1]

geoIP_city = GeoIP.open("/usr/local/share/GeoIP/GeoLiteCity.dat",GeoIP.GEOIP_MEMORY_CACHE)
geoIP_country = GeoIP.open("/usr/local/share/GeoIP/GeoIP.dat",GeoIP.GEOIP_MEMORY_CACHE)
geoIP_asn = GeoIP.open("/usr/local/share/GeoIP/GeoIPASNum.dat",GeoIP.GEOIP_MEMORY_CACHE)
geoIP_organization = GeoIP.open("/usr/local/share/GeoIP/GeoIPOrg.dat",GeoIP.GEOIP_MEMORY_CACHE)

if os.path.isfile(IP):
	as_file(IP)
else:
	if "/" in IP:
		rangeIP=list_ips(IP)
		for ip in rangeIP:
			geo_locate(ip)
	else:
		geo_locate(IP)

