# Define map of IDS signatures to VERIS actions
# 
def initialize_map(sig_map):
    sig_map = dict()
    
    # TODO: include comments into description field
    sig_map['NID-SEAM-VIRUS_FOUND'] = { 'category': 'Malware distributor', 'actions': ''}
    sig_map['NID-TPIDS-13012'] = { 'category': 'Malicious host', 'actions': 'Hacking: Brute force'} # SipVicious brute force
    sig_map['NID-ISS-TCP_Network_Sweep'] = { 'category': 'Malicious host', 'actions': 'Hacking: Footprinting'}
    sig_map['NID-DRAGON-WEB_SUSP_USER_AGENT_BOT'] = { 'category': 'Bot', 'actions': ''}
    sig_map['NID-TPIDS-11105'] = { 'category': 'Malicious host', 'actions': 'Hacking: Forced browsing'} # "11105: HTTP: Suspicious Request for an Executable (Possible Malware Download Attempt)"
    sig_map['NID-TPIDS-9220'] = { 'category': 'Malicious host', 'actions': 'Hacking: Unknown'} # "9220: PHP: Malicious Obfuscated PHP Program Access"
    sig_map['NID-ISS-HTTP_Cisco_IOS_Admin_Access'] = { 'category': 'Malicious host', 'actions': 'Hacking: Brute force'} # "Cisco IOS HTTP server could allow unauthorized administrative access"
    sig_map['NID-TPIDS-2556'] = { 'category': 'Malicious host', 'actions': 'Hacking: Other'} # "2556: HTTP: HTTP CONNECT TCP Tunnel to SMTP port"
    sig_map['NID-TPIDS-6226'] = { 'category': 'Bot', 'actions': 'Hacking: SQLi' } # "6226: HTTP: SQL Injection Tool with Asprox Botnet"
    sig_map['NID-ISS-HTTP_CGI_Fastgraf'] = { 'category': 'Malicious host', 'actions': 'Hacking: OS commanding'} # "Fastgraf CGI scripts allow remote command execution"
    sig_map['NID-TPIDS-5669'] = { 'category': 'Malicious host', 'actions': 'Hacking: SQLi'} # "5669: HTTP: SQL Injection (UNION)"
    sig_map['NID-TPIDS-3809'] = { 'category': 'Malicious host', 'actions': 'Hacking: SQLi'} # "3809: HTTP: SQL Injection Evasion SQL Comment Terminator"
    sig_map['NID-TPIDS-3593'] = { 'category': 'Malicious host', 'actions': 'Hacking: SQLi'} # "3593: HTTP: SQL Injection (UNION)"
    sig_map['NID-TPIDS-13094'] = { 'category': 'Malicious host', 'actions': 'Hacking: RFI'} # "13094: PHP: Parallels Plesk Remote File Includes Vulnerability"
    sig_map['NID-SNORT-1_15183'] = { 'category': 'Malicious host', 'actions': 'Social: Unknown'} # "POLICY-SOCIAL Yahoo messenger http link transmission attempt"