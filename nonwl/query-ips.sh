#!/bin/bash

for f in `ls *.zip`; do
	unzip $f
done

cut -d, -f 2 *.csv | sed '/[a-zA-Z:]/d' | sort -u > $$-ip.lst

echo "IP addresses in $$-ip.lst"
