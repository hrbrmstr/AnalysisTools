AnalysisTools
=============

Collection of scripts &amp; such used for analysis

## Current Tools
* scavenge.py - Used for gathering data about addresses (IP or domain name) without having to check multiple sources. For usage examples, check the script header. **NOTE:** Be sure to change your IOCDB login information in the script. You will also be required to enter our API Keys for VT & pDNS in analysis.cfg.
* SinkCheck.py - This tool will query the Abuse.ch intel data for known Abuse.ch sinkholes. It can take in a single IP, or use the 'SinkCheck.py bulk -i INPUT.txt -o OUTPUT.txt' form.
* watchlist/listparse.py - Used to parse WL data from VZ MSS
* nonwl/listparse.py - Used to report on data from VZ MSS observations based on IDS, not watchlist

## Known issues
* When performing bulk queries, the input file must be in your working directory or IOCDB queries will not function.
