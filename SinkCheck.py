#!/usr/bin/env python

import sys
import subprocess
import argparse

def map(ip):
	host = '.wl.intel.abuse.ch'
	reverse = ip.split('.')
	mapped = reverse[3]+'.'+reverse[2]+'.'+reverse[1]+'.'+reverse[0]+host

	cmd = 'dig +short '+mapped+' TXT'

	process = subprocess.Popen(cmd, shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = process.communicate()

	return out

def main():
	parser = argparse.ArgumentParser(description="Quick Utility to grab TXT records from AbuseCH Intel data for identifying sinkholes")
	parser.add_argument('ip', nargs=1, help='The IP to Scavenge, use "bulk" for a file-based lookup')
	parser.add_argument('-i', '--input', help='Conduct a bulk lookup from file')
	parser.add_argument('-o', '--output', help='Specify an output file for the report')
	args = parser.parse_args()
	 
	results = []

	if not args.ip[0]:
		print "Please specify an IP address to query"
		sys.exit()
	elif args.ip[0]=='bulk':
		if not args.input:
			print "Please specify an input file using the -i switch"
			sys.exit()
		else:
			ifile = open(args.input, 'r')
			for lin in ifile:
				entry = map(lin.strip())
				if entry == '':
					pass
				else:
					pair = [lin.strip(), entry.strip()]
					results.append(pair)
	else:
		entry = map(args.ip[0])
	
	if args.output:
		ofile = open(args.output, 'w')
		if not results:
			ofile.write('IP: '+args.ip[0].strip()+' Entry: '+entry)
		else:
			for hit in results:
				ofile.write('IP: '+hit[0]+' Entry: '+hit[1])
		ofile.close()
	else:
		if not results:
			print 'IP: '+args.ip[0].strip()+' Entry: '+entry
		else:
			for hit in results:
				print 'IP: '+hit[0]+' Entry: '+hit[1]

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		sys.exit()